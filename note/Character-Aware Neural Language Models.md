# [Character-Aware Neural Language Models](https://arxiv.org/abs/1508.06615)

这是一篇主要服务于代码实现的笔记，边看论文边写的。记录我的疑问和实现代码，并提供一个关于论文的简要结构，便于回忆。

## 模型

这是一个基于character embedding的语言模型,模型图示如下

![model](paper_img/char_model.png)

我关心的是3个没有了解和实验的部分:(主要是2和3)

1. Concatenation of character embeddings
2. Convolution layer with multiple filter of differenct widths + max polling layer
3. Highway network

### character embeddings

为了后续讨论的确切，定义一些用到的符号:

- $\mathcal C$：vocabulary of characters.
- $d$: dimensionality of character embeddings.
- $\bold Q$: the matrix character embeddings. $\bold Q \in \mathbb{R}^{d \times |C|}$

这几个定义都比较直观，熟悉word embedding的很容易理解。

- $\mathcal V$: vocabulary of words.
- $k$: word $k \in \mathcal V$, is made up of a sequence of characters $[c_1, ..., c_l]$

由此，对于每个word $k$, 可以有一个character level representation:

$C^k \in \mathbb{R}^{d \times l}$.

这里需要注意，$l$是word $k$的字符数量。但是不同的words的长度不一样，为了可以批处理（一次进行多个句子的训练），我们定义

$m$: max word length (包含 start-of-word, end-of-word)

这样word $k$的 character level representation变为:

$C^k \in \mathbb{R}^{d \times m}$

#### 代码部分

首先我们需要从训练的数据集中获得字符集合$C$。在这个项目中，训练数据集为`train.context` `train.question`。`train.answer`是来自于`train.context`的，就不用处理了。

```python
def read_vocabulary_of_characters(file, upper_limit_of_rare_character, characters_ignored=( '\n', ' ' )):
    """read vocabulary of characters from a list of dataset.

    Args:
        file: data file. e.g. train.answer, train.context,...
        upper_limit_of_rare_character: if the count of character is not greater than this, then it will be removed
        characters_ignored: list of ignored characters
    Returns:
        A list of character
    """
    # consider the file size, we can read the whole file into memory.
    characters = defaultdict(int)
    for line in file:
        for char in line.decode(encoding='utf8'):
            characters[char] += 1

    # remove ignored characters
    for char_ignored in characters_ignored:
        characters.pop(char_ignored)

    # remove rare characters
    rare_characters = []
    for char in characters:
        if characters[char] <= upper_limit_of_rare_character:
            rare_characters.append(char)
    for char in rare_characters:
        characters.pop(char)

    return characters
```

我们只需要做一遍这样的构建字符集，因此将获得的字符集写入文件中，便于之后读取。

最后为了构建$k$对应的character level representation $C^k$，我们还要建立char2id的查询表，以及character embedding matrix $\bold Q$。另外除了从数据集中获取的character外，我们需要几个特殊的字符:

```python
_CHAR_PAD = b"<char_pad>"
_CHAR_UNK = b"<char_unk>"
_CAHR_START = b"<word_start>"
_CHAR_END = b"<word_end>"
_CHAR_START_VOCAB = [_CHAR_PAD, _CHAR_UNK, _CAHR_START, _CHAR_END]
CHAR_PAD_ID = 0
CHAR_UNK_ID = 1
CHAR_START_ID = 2
CHAR_END_ID = 3
```

获取embedding matrix 和 char2id的代码

```python
def get_characters(characters_path, char_dim):
    """Reads from original characters.txt file and returns characters embedding matrix and
    mappings from characters to character ids.

    Args:
        characters_path: path to characters.txt
        char_dim: dimension of character embedding vector

    Returns:
        char_emb_matrix: Numpy array shape(len(characters) + len(_CHAR_START_VOCAB), char_dim)
        char2id: dictionary mapping char (string) to char id (int)
        id2char: dictionary mapping char id (int) to char (string)
    """
    char2id = {}
    id2char = {}

    # put start tokens in the dictionaries
    idx = 0
    for char in _CHAR_START_VOCAB:
        char2id[char] = idx
        id2char[idx] = char
        idx += 1

    with open(characters_path, 'r') as f:
        for line in f:
            char = line.strip()
            char2id[char] = idx
            id2char[idx] = char
            idx += 1

    # Random initialize character embedding matrix
    char_emb_matrix = np.random.randn(idx + 1, char_dim)

    return char_emb_matrix, char2id, id2char
```

在有了char_emb_matrix, char2id之后，我们还需要：

```python
# 1. 在batch中增加character_ids
def tokens_to_ids(tokens, word2id, char2id, using_start_end_id):
    pass

def padded_characters(char_ids_batch, batch_pad, max_word_length, using_start_end_id):
    pass

# 2. 添加 character_ids 的placeholder
self.context_ids = tf.placeholder(tf.int32, shape=[None, self.FLAGS.context_len])

# 3. feed character_ids
input_feed[self.context_ids] = batch.context_ids

# 4. 创建一个可训练的character embedding matrix
char_embedding_matrix = tf.get_variable('char_embeeding', initializer=tf.constant(char_emb_matrix, dtype=tf.float32), trainable=True)

# 5. embedding_lookup 可以得到character level representation C^k. 
context_char_embs = embedding_ops.embedding_lookup(char_embedding_matrix, self.context_char_ids) # shape (batch_size, context_len, character_len, char_embeeding_size)
```

### convolution layer + max-over-time pooling layer

输入为上一层获得的character level represention，输出将是character level的 word embedding vector. 同样地还是围绕word $k$进行描述。

1.首先需要一个矩阵filter(or kernel)：

$$
  \bold H \in \mathbb{R}^{d \times w}. w \ \text{is the width of filter}
$$

2.接着对$C^k$和$H$进行卷积操作:

$$
\langle C^k[..., i:i+w-1], \bold H \rangle \quad \text{for} \quad i \in \{0,...m-w\}
$$

其中$m$是我们之前定义的最大单词长度，内积的定义如下
> Forbenius inner product:
>
>$\langle A, B \rangle = Tr (AB^T)$ 这等价于 $\sum_i \sum_j A_{ij} B_{ij}$

3.然后加上一个非线性的映射:

$$
f^k[i] = \tanh(\langle C^k[..., i:i+w-1], \bold H \rangle + b)
$$

4.最后 max-over-time, 得到

$$
t^k = max_i f^k [i]
$$

使用了$h$个filters，得到$y^k = [t_1^k, t_2^k, ..., t_h^k]$

#### 总结

($C^k$ 和 filter $H$)卷积 -> 非线性 -> max over time -> k 的feature $t^k \in R$ . 

$h$个filters, 得到 $y^k= [t_1^k, t_2^k, ..., t_h^k] \in R^h$

#### 代码部分

非线性在tensorflow中 `tf.layers.conv1d`的参数中体现，我们采用了论文中的非线性函数`tanh`以及`kernel_size=5`，`filters=100`

```python
def word_char_level_embedding(inputs, word_embedding_size, kernel_size=5, activation=tf.tanh, use_bias=True):
    """通过卷积，非线性，和最大池化，得到每个word token在character level上的embedding

    Args:
        inputs: Character level representation. Tensor shape(batch_size, seq_len, character_len, char_embedding_size)
        word_embedding_size: Int. The embedding vector size for each word
        kernel_size: An integer or tuple/list of a single integer, specifying the
          length of the 1D convolution window.
        activation: Activation function. Set it to None to maintain a
          linear activation.
        use_bias: Boolean, whether the layer uses a bias.

    Returns:
        outputs: Word Character level embedding. Tensor shape(batch_size, seq_len, word_embedding_size)

    """
    _, seq_len, char_len, char_embedding_size = inputs.shape.as_list()
    inputs = tf.reshape(inputs, shape=(-1, char_len, char_embedding_size))
    outputs = tf.layers.conv1d(inputs,
                               filters=word_embedding_size,
                               kernel_size=kernel_size,
                               activation=activation,
                               use_bias=use_bias)
    word_embedding = tf.reduce_max(outputs, axis=-2)
    return tf.reshape(word_embedding, shape=(-1, seq_len, word_embedding_size))
```

### Highway network

TODO：暂时没有加入代码中的计划。

## 问题

1. 是否需要从dev数据集中获取字符集合？又或者直接从外部寻找一个英文可能用到的所有字符集合？

2. 从`train.answer`中读取了1257个字符，是否需要过滤极少出现的字符？

## References
[^1]: https://arxiv.org/abs/1507.06228
[^2]: https://arxiv.org/abs/1508.06615