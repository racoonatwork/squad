一些实验结果, 括号内为experiments_name

## character embedding + modeling layer

对比之前简单的 bi-directional attention model, 按照
[Bidirectional Attention Flow for Machine Comprehension
](https://arxiv.org/abs/1611.01603)添加了 Character Embeddling Layer 和 Modeling Layer， 效果对比如下

### 简单模型 (bidaf_medium)

- dev/EM: 0.3442, 17k steps

### 目前模型 (bidaf_medium)

- dev/EM : 0.5080, 7k steps
- dev/F1: 0.6580, 7k steps

提升还是可以的。但是和论文中的 EM 67.7，F1 77.3 还差了不少。除了缺少highway区别已经不大了。

## 在character embedding后添加highway

2.5k steps ，dev/EM 才0.2563, 比之前同样steps的0.4259少了太多，highway的实现有问题？还要继续观察