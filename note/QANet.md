# [QANet: Combining Local Convolution with Global Self-Attention for Reading Comprehension](https://arxiv.org/abs/1804.09541)

复现[QANet](https://arxiv.org/abs/1804.09541)的一些总结

## QANet的结构

和之前的QA model which are primarily based on RNNs with attention 不同，QANet的building blocks 不包含RNNs，而是convolutions and self-attentions，从而获得更快的训练速度(3x - 13x faster)和推理速度(4x - 9x faster)。由于可以更快的训练，所以model可以在更大的数据集上获得了更好的F1-score(84.6).

通过将句子翻译成法语再翻译回来获得更多的数据

## Five Layers

### 1. Input Embedding Layer

对每个单词$w$标准处理流程：
Concatenate word embedding and character embedding.

#### Word Embedding

- $p_1 = 300$ dimensional Pre-trained GloVe word vectors. (Fixed, non-trainable)
- Out-of-vocabulary words are mapped to an `<UNK>` token. (Random Initialized, trainable)

#### Character Embedding

所有的字符嵌入都是trainable的$p_2 = 200$的向量，而每个$w$都截断或者填充成16个字符，由此我们获得了一个$200 \times 16$的矩阵$M$。 对$M$每行取最大值，得到了一个$p_2$大小的字符嵌入向量。

### 2. Embedding Encoder Layer

### 3. Context-Query Attention

### 4. Model Encoder Layer

### 5. Output Layer

## [Depthwise Separable Convolutions for Neural Machine Translation](https://arxiv.org/abs/1706.03059)

depthwise convolution: 
对各个channel分别进行一个空间上的卷积

pointwise convolution

```python
def separable_conv2d(input,
                     depthwise_filter,
                     pointwise_filter,
                     strides,
                     padding,
                     rate=None,
                     name=None,
                     data_format=None):
  """2-D convolution with separable filters.

  Performs a depthwise convolution that acts separately on channels followed by
  a pointwise convolution that mixes channels.
```

> the fundamental idea behind depthwise separable convolutions is to replace the feature learning
operated by regular convolutions over a joint "space-cross-channels realm" into two simpler steps, a
spatial feature learning step, and a channel combination step. This is a powerful simplification under
the oft-verified assumption that the 2D or 3D inputs that convolutions operate on will feature both
fairly independent channels and highly correlated spatial locations.

也就是说假定不同的channels之间互相独立，空间上高度相关，就可以使用这种方法来减少参数和计算量。就NLP上来说，channels对应我们的嵌入向量的分量，可以认为各个分量之间独立，而嵌入本身之间是有上下文联系，有位置上的高度相关。

在减少参数方面，就我们的模型而言：如果原有输入为(m, l, d), 需要$k$个filters，$w$的kernel_size

## inverse exponential increase

> We use
a learning rate warm-up scheme with an inverse exponential increase from 0.0 to 0.001 in the first
1000 steps, and then maintain a constant learning rate for the remainder of training.

在之前的实验中(简单的Transformer Encoder + bidaf), 我发现如果使用learning rate=0.001作为Adam optimizer的参数，loss会下降一点就不再变化。

## Exponetially weighted average

EMA是否应该只用在predict而不是train上？如果在train的时候用平滑的变量代替当前的，会怎么样？在还远未收敛的时候，模型很难训练？

EMA的作用就是为了消除收敛阶段的波动？能够得到更合适的model参数？

[Coursea 教程](https://www.coursera.org/lecture/deep-neural-network/exponentially-weighted-averages-duStO)

### `tf.train.ExponentialMovingAverage`怎么使用？

```python
apply() -> op
train() -> update op + assign_op
test -> run(assign_op)
```

### context-query attention

更节约内存的计算similarity方式，我觉的最有趣的是计算`c*q`的部分：（比起原来的做法，需要的空间复杂度 $|C| \times |Q| \times d$ -> $|C|\times d + |Q| \times d$）

```python

```

其和`expand_dim`后[Hadamard product/每个元素相乘](https://en.wikipedia.org/wiki/Hadamard_product_(matrices))再矩阵相乘的等价性可以通过比较最终结果的下标来验证：

![手写潦草计算](https://note.youdao.com/yws/api/personal/file/24c11d4bfea07f715920c6d16f7b809f?method=download&shareKey=6652cde0d1e60082c0da35bb8665d387)

### [Stochastic depth method (Layer dropout)](https://arxiv.org/abs/1603.09382.pdf)

为了消除deep network难以训练的问题，随机的drop掉一些layer。不同于dropout让网络变的`thin`, stochastic depth让网络变得`shorter`. 还可以将stochastic depth看作是ensemble 不同深度的网络。

在QANet中，每个block中都包含数个residual blocks,

问题： `L`是单个stacked block中，整个encoder layer，还是整个网络的residual block的数量?

## References

- [Depthwise Separable Convolutions for Neural Machine Translation](https://arxiv.org/abs/1706.03059)
- [Attention is All You Need](https://arxiv.org/abs/1706.03762)
- [QANet: Combining Local Convolution with Global Self-Attention for Reading Comprehension
](https://arxiv.org/abs/1804.09541)
- [Deep Networks with Stochastic Depth](https://arxiv.org/abs/1603.09382.pdf)