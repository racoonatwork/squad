# 问题集

汇总一些碰到和想到的问题。

## 为什么在低于50%的EM之前， F1和EM的差距都大到15%左右？而leadboard前列的F1和EM相差都在6%左右？

是因为相当一部分预测不准时，EM要完全匹配，所以会偏低;而F1则不同，只要能覆盖到部分答案就有提升。

## 为什么LSTM加了layer normalization 训练速度差的那么多？和不加layer norm时大致有5-6倍差距？（18s+ -> 3.2s)

## learning rate和模型的大小有关系吗？和模型的参数初始化有什么样的关系？为什么基于transformer的模型，需要的lr差bidaf的1-2个数量级？

## 第一次batch time为什么在GPU上要很久，大概3,4分钟？在我的笔记本上和其他的batch时间差不多？

## 为什么论文里说bert是bi-directional, GPT是单向的，我看好像都是堆叠encoder呀？

GPT使用了decoder，Attention时有考虑用mask来阻止关注前面的位置(TODO, 具体点), 而 BERT 使用的encoder，没有这种约束，每一个单词都可以关注整个上下文，所以可以说是bi-directional,我个人也认同下面的说法应该叫non-directional.

> As opposed to directional models, which read the text input sequentially (left-to-right or right-to-left), the Transformer encoder reads the entire sequence of words at once. Therefore it is considered bidirectional, though it would be more accurate to say that it’s non-directional.
>
> <https://www.lyrn.ai/2018/11/07/explained-bert-state-of-the-art-language-model-for-nlp/>

> We note that in the literature the bidirectional
> Transformer is often referred to as a “Transformer
> encoder” while the left-context-only version is re-
> ferred to as a “Transformer decoder” since it can
> be used for text generation.
>
> [bert](https://arxiv.org/abs/1810.04805)

## 如果loss的曲线比较平滑，是不是绝大可能性已经快收敛了或者过拟合了？

## bidaf 的 Attention实现是否有错？

bidaf模型在17k steps之后也只有50的EM，与论文中的67.7差的有点多，F1是65.8对77.3。除了没有HighWay和`moving averages of all weights`之外，差别已经不大，原因出在哪里？是否Attention机制有误？曲线已经很平滑，继续训练能否有提高？

最容易出错的地方：**Query-to-context Attention**, 对应代码里 **Value-to-Key Attention**。 之后为了便于讨论，只使用`Context`,`Value`，而不是代码中的`Key`,`Value`，并且`Context`,`Value`都特指单词构成的串而不是某个单词。

之前我在写代码时，没有对这部分进行彻底的思考。我的思路是：为了从`Similarity Score Matrix` $\bold S$中获得`Context`的分布，我需要得到一个Shape和`Context`长度一样分量，以便于`softmax`函数能够得到基于`Context`的分布。因此我基于[paper](https://arxiv.org/abs/1611.01603)中的$max_{col}(\bold S)$,直接对$\bold S$的最后一个axis执行 `reduce_max`，以得到想要的tensor shape。

这里有一个问题，[论文](https://arxiv.org/abs/1611.01603)中是这样描述的
> where the maximum function (max col ) is performed across the column.

文中的 $\bold S \in \mathbb{R}^{T \times J}$ 和代码的中的Tensor形状一致，那么我执行的操作不是 max across the row吗？

不管是我的理解有误（也许 across the column针对的是文中的图片?)，还是文中描述错误。**最关键的问题是：我们应该如何得到一个长度为`Context`的score列表？其中每个score的大小能反映出这个`Context`对应位置的单词对于`Query`的重要程度？** 我不能仅仅只靠Shape来完成代码，而不去检查这一想法是否得到了实现。

我觉得这个和pooling有些共通的地方，也许可以用`mean`而不是`max`，这样在`axis=-1`也就是row上取得这个`Context`的单词对于整个query的平均score，也就是平均重要程度。当然`max`可能更能体现重要程度，因为一个句子中可能有很多词作用不大（比如`the`, `and`等)，需要关注特别突出的。（TODO,实验一下mean的效果)

代码如下，经过分析，觉的没有啥问题。（1500步左右loss下降很慢让我觉的哪里有问题，也许只是character embedding加上2层highway后需要更多的步骤训练）

```python
# Value-to-key attention. 对于values(query)的keys(context)的权重和
# Find the max value for each key.(each row of similarity matrix)
max_score_each_key = tf.reduce_max(masked_logits, axis=-1) # shape(batch_size, num_keys)
# Get an attention distribution over the keys.
_, v2k_attn_dist = masked_softmax(max_score_each_key, mask=keys_mask, dim=-1) # shape(batch_size, num_keys)
# Take the weighted sum of the keys as output.
v2k_output = tf.matmul(tf.expand_dims(v2k_attn_dist, axis=1), keys) # shape(batch_size, 1, key_vec_size)
```

### 卷积中channels指的是什么？

常见的RGB channels

- N: number of images in the batch
- H: height of the image
- W: width of the image
- C: number of channels of the image (ex: 3 for RGB, 1 for grayscale...)

### 如果已经在很多需要regularizer参数的layer都添加了l2,最终l2_loss还需要添加吗？

本来我是认为不需要添加l2_loss了，但是有一个和我一样的疑问[How to add regularizations in TensorFlow?
](https://stackoverflow.com/questions/37107223/how-to-add-regularizations-in-tensorflow)，下面给出的解答是，需要添加l2_loss

但是我觉得这样的设计是为了什么？为了控制regularization的行为吗？最后不添加l2_loss就没有regularize?添加了就有？

另外关于最高和第二高的答案正确性我也觉得怀疑，像评论里说的一下，是否相当于做了2次regularize。通过阅读代码我最终选择加上`tf.losses.get_regularization_loss()`。

需要验证这个做法的正确性（通过添加l2_loss到summary中，在tensorboard中观察）

在`transformer_qanet`上实验的结果 (tensorflow-1.4.1)

func | l2-loss
--- | ---|
get_regularization_loss | 1.3e-3
apply_regularization | 1.7e-13


stackoverflow上的高票做法:

```python
reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
reg_constant = 0.01  # Choose an appropriate one.
loss = my_normal_loss + reg_constant * sum(reg_losses)
````

```python
reg_variables = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
reg_term = tf.contrib.layers.apply_regularization(regularizer, reg_variables)
loss += reg_term
```

## 在满足`start <= end`的约束下，如何在numpy里如何写动态规划算法来查找最大的(start, end)

本身算法是很简单的，只要遍历2次数组：一个从左到右的每个子区间的最大`start`位置，一个从右到左的每个子区间的最大`end`位置。然后再遍历数组中的每个位置，得到每个位置的最大(start, end)概率，就可以得到最大的`(start, end)`，这样算法可以在线性时间内求解。

但在numpy中做loop，会不会效率不高，还不如采用矩阵化的方式来进行？[Speeding up dynamic programming in python/numpy](https://stackoverflow.com/questions/20428541/speeding-up-dynamic-programming-in-python-numpy)这个提问和我的疑惑是差不多的。

## initializer要如何选择

## dropout为什么有用

## layer dropout / stochastic depth 为什么有用/没用

## MutliHead哪个部分导致dev EM提升很慢？

根据实验`qanet_debug_345_multihead`，在更换mutli_head_attention后，5.0K steps时 EM: `0.2868->0.4577`。

经查证，主要是scaledDotProduct

首先，根据[transformer论文](https://arxiv.org/abs/1706.03762)中的表述，多个heads有助于学习到多个不同子空间的信息

## 什么情况下需要bias，什么情况下不太重要？或者干脆不管什么情况都加上bias，这样模型的表达能力肯定更强？

## Multi-Head Attention 在做些什么？

## Position-wise Feed-Forward Networks的做法: tf.layers.dense 和 kernel_size=1 output_size=unit的conv1d 有区别吗？