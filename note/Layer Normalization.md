# [Layer Normalization](https://arxiv.org/pdf/1607.06450.pdf)

> In this paper, we transpose batch normalization into layer normalization by
computing the mean and variance used for normalization from all of the summed
inputs to the neurons in a layer on a single training case.

## 与Batch Normalization的不同

Batch Normalization(BN)在一个neuron上的多个训练样本上进行标准化， 而Layer Normalization(LN)则是在一层上对一个训练样本进行标准化.

## 为什么要这么改进

RNNs的输入句子通常都是变长的这样无法在mini-batch上进行标准化。

## LN的做法

符号：
- $h^l$: 第l层的输入
- $a_i^l$: l层第i个neuron的输入和, $a_i^l = {w_i^l}^T h^l$`
- $H$: 第l层的neuron的数量

### 在RNNs中的应用