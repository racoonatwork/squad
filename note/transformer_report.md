# 一个阶段性的总结报告

每个标题为日期 + `git tag`， 其中`git tag`也会是experiment name

基本的transformer baseline的模型，对应的git tag为
`transformer_baseline_dev_em31_f1_49`。从tag名字可以看出，在squad1.1的dev数据集上的F1为49%，EM为31%。总的来说结果不好, F1虽然超过了之前bidaf的46%,但是EM却没有bidaf的34%高，具体原因还有待调查，但作为baseline验证了一下代码的正确性。

## 目前最佳

experiment_name :`qanet_debug`

dev EM: 62.87 F1:73.17


## 2019-3-10 transformer_qanet

QANet的结构类似，但是结果不太理想

### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet
num_parameters | 2923970
batch_size | 32
batch_time | 0.42

### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- | --- | --- | --- | -- |
25 | 0.45 | 0.60 | 0.68 | 0.81

TODO 图片结果

### 讨论

TODO l2 loss 图片

l2使用 `tf.losses.get_regularization_loss()`，为什么显示成一条直线？放大后发现也不是完全笔直。

## 2019-3-11 transformer_qanet_l2loss

由于上个版本的overfit严重，怀疑l2-loss的代码是不是有问题，修改l2-loss如下,其余保持不变。

```python
variables = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
regularizer = tf.contrib.layers.l2_regularizer(scale=self.l2_lambda))
self.l2_loss = tf.contrib.layers.apply_regularization(regularizer, variables)
```

### 结果

然而这样修改之后，l2_loss 由原来`1.3e-3`变为`1.7e-13`，说明被乘了2次，并且导致`grad norm=inf`, `loss=7.2e+8`,无法训练。 为什么网上的代码很多是这样的没有问题？

## 2019-3-11 transformer_qanet_layer_dropout

基于transformer_qanet，增加了layer dropout 和 word embedding的dropout

### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_layer_dropout
num_parameters | 2923970
batch_size | 32
batch_time | 0.41

### 结果，10.5k steps

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
10.5 | 0.296 | 0.424 | 0.345 | 0.463

### 讨论

从各种指标的曲线上看，都弱于没有加layer dropout的，包括dev的指标，所以在10.5ksteps之后没有继续。

## 2019-3-12 transformer_qanet_glove_char

基于上个版本，将trainable的character embedding更换为 glove.840B.300d-char.txt.

### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_glove_char
num_parameters | 3317066
batch_size | 32
batch_time | 0.415

### 结果

705 steps之后，出现了`grad norm=inf`,和之前的l2-loss差不多，但是这里的l2-loss已经改回原来的，说明和l2-loss没有关系，而是原来的`transformer_qanet`就有可能出现这个问题？

~~（经过debug，已经修复，和代码无关，而是conda安装环境的问题!!! aliyun的ECS是镜像制作的，上面没有conda，后来我为了训练bert和QANet，安装了conda和各种tensorflow，也许是不同版本的tensorflow依赖的库导致了该问题，具体原因无法查明。在`base`上执行`conda install tensorflow-gpu=1.4.1`，并删除无用的tensorflow，暂时没有重现了）~~

刚刚说完上面的话，就重现了，准备禁用一下ema试一下。这下彻底糊涂了，因为在`debug`分支里，启用过ema，而且ema部分代码简单，不应该引起这个问题？（和ema无关)

比较`debug`分支的历史，在替换`context-to-query`部分代码后，没有出现过这个`grad norm`爆炸的问题，是否该部分代码有问题？

更新： 自从使用 DynamicCoAttention之后，没有再复现。

## 2019-3-13 transformer_qanet_coattn

替换了co_attention

### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_glove_char
num_parameters | 3317066
batch_size | 32
batch_time | 0.399

### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
10.5 | 0.2816 | 0.3992 | 0.3370 | 0.4563
30.0 | 0.3606 | 0.4908 | 0.5730 | 0.6998

### 讨论

同样数量参数，快了0.16秒，说明co-attention实现简单，效率高。

比起论文中3个小时就在dev上得到了77的F1,我的还不到40,哪里有严重的实现问题？考虑是输入的embedding环节有问题？或者在测试dev的时候还带着dropout？又或者character的pad有问题？都需要测试，重点坚持和dev相关的内容

现在的问题有2个：

1. 过拟合，dev的EM上升的太慢，目前在排除影响dev EM低的原因。测试dev的时候不应该带着layer_dropout。。。我想到了这个原因，需要实验。
2. train的 F1和EM上升的也慢，但是在没有layer_dropout之前，上过81+，这可能表明character出问题的几率不大？

## 2019-3-13 qanet_dev_no_drop

增加了一个placehodler, 使得在dev上推理时不再drop layer

### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_glove_char
num_parameters | 3317066
batch_size | 32
batch_time | 0.399

### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
10.5 | 0.2378 | 0.3533 | 0.2820 | 0.3911

### 讨论

问题：

1. 反而每个steps各种F1和EM比之前更低（大概5%左右，慢一拍)，dev loss也没有改善，overfit依旧
2. 训练还是很慢，train和dev loss降低太慢

试一下各种方案：

1. 减少参数。将multi-attention里的linear去掉， 将character embedding换成trainable的，不用pretrain的，pretrain的embedding size太大，还有character的200减少到100。 Highway不需要non-linear
2. 更换initializer，relu不依赖xavier
3. drop次数减少一些,每2层conv drop一次

## 2019-3-14 qanet_less_params

### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_glove_char
num_parameters | 2005906
batch_size | 32
batch_time | 0.392

### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
10.5 | 0.2553 | 0.3764 | 0.3060 | 0.4149

### 讨论

参数减少了100W，batch_time几乎没有变化，仅仅减少0.01不到

## 2019-3-14 qanet_simple

继续减少参数:

- 删除了multi-head中的linear
- scaled dot product简化，不再使用scaled
- 按照论文kernel_size 在model encoder中取5

### 结果

### 讨论

没有效果

## 2019-3-15 qanet_reuse

### 参数

parm | desc |
--- | --- |
experiment_name | qanet_reuse
num_parameters | 1280598
batch_time | 0.238
num_heads | 1

### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
10.5 | 0.2553 | 0.3764 | 0.3060 | 0.4149

### 讨论

参数减少了80W,num_head=1,也大大的减少了batch_time,但效果也随之变的更低了，没有比较价值。

#### 比较

对比一下
[用卷积神经网络和自注意力机制实现QANet（问答网络）
](https://blog.csdn.net/fendouaini/article/details/82839010)中的训练图

![result](http://www.tensorflownews.com/wp-content/uploads/2018/09/9.png)

## 2019-3-12 分支 debug

为了定位问题，我从一个开源的[QANet](https://github.com/NLPLearn/QANet)实现上，用类似代码替换我的代码。仅仅为了debug，没有保存相应的模型，标题名字也没有记录tag

### MultiHead

先从最有可能出问题的 multihead开始

#### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_debug_multihead
num_parameters | 3249482
batch_size | 32
batch_time | 0.362

#### 结果

2466 steps之前，和之前的结果差不多， 但是2466步之后， grad_norm 越来越大，直到溢出，导致loss也变得很大。

#### 讨论

训练速度：我的mutlihead实现不够效率，比上个实验略少的参数3.31M->3.25M, 每个batch_time: 0.415->0.362, 慢了一些。

### Highway

去掉l2loss, 并替换highway

parm | desc |
--- | --- |
experiment_name | transformer_qanet_debug_multihead
num_parameters | 3317066
batch_size | 32
batch_time | 0.362

#### 结果

2440步`grad_norm`和`loss`爆炸了，并且我checkout到`transformer_qanet_layer_dropout`， 从之前训练的10.5k继续，不到10.7k也爆炸了;即便换为最早的`transformer_qanet`，训练不到840步也出现了一样的情况。

#### 讨论

这个情况看起来和代码没有什么关系，想到的最有可能的情况是我最近装了conda以及很多tensorflow版本，但是conda的隔离不应该出现问题呀？

### Context-Query Attention

恢复higyway和l2-loss, 并替换context-to-query attention

#### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_debug_c2q
num_parameters | 3317066
batch_size | 32
batch_time | 0.400

#### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
1.5 | 0.1214 | 0.2007 | 0.1240 | 0.2113
3.0 | 0.1603 | 0.2580 | 0.1750 | 0.2765
4.5 | 0.1829 | 0.2783 | 0.1870 | 0.2931
6.0 | 0.2108 | 0.3152 | 0.2530 | 0.3644
7.5 | 0.2287 | 0.3386 | 0.2510 | 0.3605
9.0 | 0.2449 | 0.3602 | 0.3060 | 0.4154

#### 讨论

恢复l2-loss对batch_time有一定的影响,
比起`transformer_qanet_layer_dropout`（没有l2-loss)，更换了DCN的Attention机制，但也许是和l2-loss有关的原因，在EM和F1上都较低。虽然过拟合的情况较小，但是由于loss曲线太过平坦，train的EM，F1甚至开始在7500步出现降低。

### Embedding_Encoder_Layer

替换了embedding encoder layer

#### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_debug_c2q
num_parameters | 3246194
batch_size | 32
batch_time | 0.409

#### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
1.5 | 0.1199 | 0.2117 | 0.1250 | 0.2167
3.0 | 0.1698 | 0.2653 | 0.2010 | 0.3032
4.5 | 0.1971 | 0.2896 | 0.2070 | 0.3151
6.0 | 0.2250 | 0.3230 | 0.2790 | 0.3797

### 讨论

dev EM，F1普遍都高1%,不过只有6.0K step，还不足说明问题，不过考虑到两者代码的不同，我认为主要体现在layer_dropout的实现上，我的代码是drop掉整个layer，这会引起网络深度的改变，可能会比较难以收敛。相比较，git上的实现是`tf.nn.dropout(inputs, 1.0 - dropout)`。

### 2019-3-18 qanet_debug_12345_v2

#### 参数

parm | desc |
--- | --- |
experiment_name | transformer_qanet_debug_12345
num_parameters | 711936
batch_size | 32
batch_time | 0.177

### 2019-3-19 qanet_debug_2345_v2

和QANet的代码相比，仅仅将InputEmbeddingLayer换成我的代码，并且将其中的highway还设置为QANet layers.highway.已进行性能对比

#### 参数

debug : 2345

#### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
2.5 | 0.2116 | 0.3012 | 0.2031 | 0.2997
5.0 | 0.5075 | 0.5798 | 0.4285 | 0.5655

#### 讨论

相比较使用我的highway，使用了layers.highway的model，在5.0k的时候dev EM和F1已经大幅领先之前2345的版本，说明我的highway性能有严重问题。

### 2019-3-19 qanet_debug_2345_drop_first

对比了一下代码，我觉得可能影响最关键的地方是dropout的时机，也许会影响highway的效果，因为highway需要建立一个通道

#### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
5.0 | 0.2749 | 0.4197 | 0.2558 | 0.4153

#### 讨论

没有什么效果，dropout先后无所谓，所以还是像QANet一样，实现一个convolutional highway看一看

### 2019-3-20 qanet_debug_2345_conv_highway

使用conv_highway 代替之前普通的highway， 其结果在5.0k左右基本无变换，说明和conv1d还是dense关系不大

### 2019-3-20 qanet_debug_2345_conv_highway_no_activation

去掉activatoin之后， 在5.0k的表现和`qanet_debug_2345_v2`已经基本一致，说明：
**Highway的非线性对于训练早期收敛速度影响巨大，对于模型的上限还未做过比较**（5.0K steps 能在dev EM上0.2887->0.5075)

### 2019-3-20 qanet_debug_345_no_layerdrop

layer_drop去掉后，在5.0k时，对tarin loss方面几乎没有影响，对dev EM和 F1有2%-3%的提升，但是会对最终结果有何影响，会不会过拟合需要进一步的观察。总结：影响不明显，不是当前影响性能的主要因素。

### 2019-3-20 qanet_debug_345_multihead

将我的multihead换乘QANet的，5.0K的指标得到了巨大提升：
dev EM 0.2868->0.4577,虽然比起`qanet_debug_2345_v2`还差5%，但是效果已经很不错，（实际上在7.5K时已经达到几乎一样的EM）这说明了**MultiHeadAttn至少极大的影响了早期收敛速度，甚至极有可能没有正确实现设计意图**

### qanet_debug_345_split

只替换split_head，保留其他我的multihead代码，发现有很多提升，在7.5k的时候和`qanet_debug_2345_v2`差4%。但是在仔细比较2个版本的split_head,我并没有发现实质差异。原因不在split_head,而是我在更换split_head的时候还做了一步，将scaled dot product部分注释掉的解注释了。

### 2019-3-20 qanet_debug_345_scaled

将scaled部分的代码注释回来，并且完全使用我的mutlihead, 结果和`qanet_debug_345_split`完全一致了，这说明**scaled dot product attention 的 scaled部分比较重要，对其长期的影响还需要进一步实验。**

这里的理论解释
> o illustrate why the dot products get large, assume that the components of q and k are independent random variables with mean 0 and variance 1. Then their dot product, $q · k = \sum_{i=1}^{d_k} q_i k_i$ , has mean 0 and variance $d_k$ .
>
> [Attention Is All You Need
](https://arxiv.org/abs/1706.03762)

而`softmax`对于相对较大的值会放大的多，导致`softmax`的梯度变得很小，难以训练。因此以后在用`softmax`进行概率分布计算时，要注意控制输入的方差，避免出现过大的数。

### 2019-3-20 qanet_debug_45

参数：779264

5.0K和`qanet_debug_2345_v2`差2.8%（0.4793， 0.5075),甚至比之前的还好一些，这说明**我的`context-to-query`没啥问题，而且只有5.0K的时候，网络的波动还是比较大的**

### 2019-3-20 qanet_debug_5

模型参数从 779264-> 487040，我的`model encoder layer`少了292224个参数，几乎减少了一半，这是哪里导致的？

由于模型参数的减少以及可能的其他的原因，5.0K时dev EM相比之前从0.4793 -> 0.2563

### 2019-3-20 qanet_debug_5_linear

为什么我增加了 一层linear， 模型参数还是 487040，这说明我的ffw没有加入模型中？

原来是由于reuse的原因，更新后，参数为524288，还是较GIT上的版本少很多。

### 2019-3-20 qanet_debug_5_bias

给separable_conv1d 和 ScaledDotProductAttention 都加上了bias，bias缺少可能会导致学习不到正确的参数吗？

参数数目为: 526544, 增加的很少，因为bias都是1d的。

我的stacked encoder reuse了不该reuse的权重？

### 2019-3-20 qanet_debug

经过阅读论文和比较代码，**我好像reuse错了范围，不是reuse 7个encoder block的，而是reuse3个stacked encoder。。。。** 我的做法让我的model layer实际上只有3层。。。
> We also share weights of the context and question encoder, and of the three output encoders.

#### 参数

debug : ""

参数总数: 795794

#### 结果

step(K) | dev_EM | dev_F1 | train_EM | train_F1 |
--- |--- | --- | --- | -- |
5.0 | 0.3333 | 0.4454 | 0.2937 | 0.4150
15.0 | 0.5612 | 0.6721 | xxx | xxx
20.0 | 0.5715 | 0.6860 | xxx | xxx

#### 讨论

和`qanet_debug_12345_v2`相比，收敛速度比较慢，最终上限可能也较低。在5.0K的时候dev EM相差14.6%， 在20K时相差6%

需要仔细研究一下`MultiHeadAttn`的设计意图和代码实现。