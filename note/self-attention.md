# [Multi-Head Attention](https://arxiv.org/abs/1706.03762)

Multi-Head Attention 是[Transfomer](https://arxiv.org/abs/1706.03762)中的Attention机制。由于发现在实现QANet时，该部分代码对模型的性能影响很大，这里总结和思考一下关于Attention相关的知识和问题:

1. Multi-Head Attention 是如何提高模型性能的？提供了哪些有用的信息？
2. Multi-Head Attention 有哪些实现方案，都是基于何种考虑？
3. 为什么我的实现效果不好？

## Overview

- Seq2Seq中的Attention回顾
- scaled dot-product attention
- Multi-Head Attention的结构
- split head
- 部分结论
- TODO
- References

## Seq2Seq中的Attention

Seq2Seq在翻译任务上取得了很大的进展，其模型主要由`encoder RNN + decoder RNN`组成。这里有一个缺陷：只将encoder最后的hidden encoding 输入 decoder，那么RNN的循环结构会导致长句子的信息丢失的多，翻译效果不好。所以我们引入了Attention机制。

Attention机制是一个同时获得所有位置信息的简单做法：

1. 将`decoder hidden state`与所有的`encoder hidden states`通过某种映射(*dot-product, additive, multiplicative*)获得`attention score/logits`。
2. 对`score/logits` 使用`softmax`获得`attention distribution`,
3. 以`attention distribution`为权重的`encoder hidden states`权重和作为 `attention output`

可能文字还是不够简单明了，所以拷贝了一张图片，来自[cs224n lecture Paying attention to attentions...](https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1184/lectures/lecture10.pdf)

![seq2seq attention](img/seq2seq_attention.png)

**Attention的定义**:
将query中
将上文中的`encoder hidden states`称为values，`decoder hidden state`称为query, 有一个更宽泛的定义:
> 给定query和values，获得values对于query的一个权重和

这种我们称为**query-to-values attention**,在代码中为 **key-to-values attention**，以和下面的**self-attention**区分开。

### 岔开的话题： 相似性度量

`score`越大，表示关注程度越高，这一点和向量方向余弦表示相似性的度量有共通的地方。我们在word2vec嵌入向量中也见过类似的`score`。我们期望类似语义的词能够在空间中比较接近，而度量方式也是内积，而且也是这样训练词向量的。

**TODO**: 查找 更多的相似性度量方法：

- [Similarity measure
](https://en.wikipedia.org/wiki/Similarity_measure)

## self-attention: scaled dot-product attention

关键的实现:

1. Mask logits: pad位置的logits要加上大的负数，以保证softmax后概率为0
2. scale 保证softmax后的输入不要太大，导致gradient太小，难以训练

### 结构

> ![Scaled Dot Product Attention](img/scaled_dot_product.png)
>
> 图片来自[Attention Is All You Need
](https://arxiv.org/abs/1706.03762)

### 一些讨论

scaled dot-product attention 和seq2seq中的attention相比，主要变化为`sacle`和`key-value pairs`。 `query`不再直接和`values`而是和`keys`计算score。这里为什么引入`key-value`，我没有找到明确的来源，个人认为可能是为了引入更通用的定义，在我的代码中和我观察过的NLP模型中，基本上`key==value`。

`self-attention`在上文对于Attention的定义下，是指:`query==key==value`，根据论文附录中的`Attention Visualizations`, 我得到的一个`self-attention`的作用解释是：**对于句子中的每个word，self-attention带来了很多关于这个word的`syntactic, sementic`的信息**。这就是神经网络以及`self-attention`机制帮助我们提取复杂特征的一个例子。

### 关于scale的实验

之前在实现QANet的过程中，将scale的部分代码注释掉之后，在同样参数的模型情况下`(num_head=1, model_size=98, batch_size=32,...)`, 5.0K的时候对比没有scaled的代码，在dev EM上相差很大（52.61 qanet_debug_345_scaled > 25.98 qanet_debug_345_no_dense 33.59)，关于这点论文中有一个解释
> To illustrate why the dot products get large, assume that the components of q and k are independent random variables with mean 0 and variance 1. Then their dot product, $q · k = \sum_{i=1}^{d_k} q_i k_i$ , has mean 0 and variance $d_k$ .
>
> [Attention Is All You Need
](https://arxiv.org/abs/1706.03762)

我的实验总结, 参见同目录下的`transformer_report.md`
> 而`softmax`对于相对很大的值会放大的多，导致`softmax`的梯度变得很小，难以训练。因此以后在用`softmax`进行概率分布计算时，要注意控制输入的方差，避免出现过大的数。

### TODO，我也需要修改一下代码，输出类似的Attention Visualizations。注意包含正确答案和错误答案的例子

## Mutli-Head Attention

### 结构

![Multi-Head Attention](img/multi_head.png)

图片来自[Attention Is All You Need
](https://arxiv.org/abs/1706.03762)

### 实践

#### split head的做法

在实践中，直接将`V,K,Q` linear transform 成$d_v * h, d_k * h, d_q * h$，然后在reshape 最后一个dimension为h份。这个做法让我联想到分块的矩阵乘法，在代码实现中，尽量矩阵化比较效率。

#### linear projection的作用

在QANet中，V=K=Q，所以我尝试过不`projection`直接split head, 为什么导致收敛效果变差？

update:

我觉得的一个可能性：不projection直接split head，等于没有学习直接对子空间进行分割，这种分割由于没有经过学习，会损害原有的向量所蕴含的上下文信息。举个不是很恰当的例子，就像一个单词只抽取其中的某几个字符一样，严重损失了信息;那么为什么linear之后可以split head呢？那是因为我们期望linear projection的过程能够学习到一些什么，不是那么粗暴的分割。当然这种学习到的是什么，也许很难解释的清楚，但直觉上来讲，可以像以下论文中提到的：

> Multi-head attention allows the model to jointly attend to information from different representation subspaces at different positions. With a single attention head, averaging inhibits this.

在QANet中，V,K,Q都是一样的，来自于上一层的 depthwise separable convolutional layer. 这一层的输出为什么需要linear之后计算attention？


另外我去掉了concat之后的linear，发现对结果影响不大，这又是为什么？

**TODO** 神经网络中每个层的意图，特别是linear的作用是在提取更高层的特征吗？特别是在这个特定的模型中缺少非常有说服力的证明，为什么少了这一层会影响很大或者又没有什么影响，选择比较有随意性，只能靠实验么?

#### 实现代码效果较差

一个是使用了[QANet
](https://github.com/NLPLearn/QANet)的mutlihead，一个是我自己的实现,其余部分代码和参数都一样。
![`qanet_debug_multihead` vs `qanet_debug`](result/multihead_vs_debug.png)

可以明显看出收敛速度差别很大,在5.0K的时候 loss相差1, dev EM相差13.3%。

## References

- [Memory, attention, sequences
](https://towardsdatascience.com/memory-attention-sequences-37456d271992)
- [Attention Is All You Need
](https://arxiv.org/abs/1706.03762)
- [Self-Attention Mechanisms in Natural Language Processing](https://medium.com/@Alibaba_Cloud/self-attention-mechanisms-in-natural-language-processing-9f28315ff905)