# coding=utf-8
# Copyright 2018 Stanford University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""This file contains some basic model components"""

from __future__ import division
import math
import tensorflow as tf
import numpy as np

from tensorflow.python.ops.rnn_cell import DropoutWrapper
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import rnn_cell

initializer = lambda: tf.contrib.layers.variance_scaling_initializer(factor=1.0,
                                                             mode='FAN_AVG',
                                                             uniform=True,
                                                             dtype=tf.float32)
initializer_relu = lambda: tf.contrib.layers.variance_scaling_initializer(factor=2.0,
                                                             mode='FAN_IN',
                                                             uniform=False,
                                                             dtype=tf.float32)
regularizer = tf.contrib.layers.l2_regularizer(3e-7)



class RNNEncoder(object):
    """
    General-purpose module to encode a sequence using a RNN.
    It feeds the input through a RNN and returns all the hidden states.

    Note: In lecture 8, we talked about how you might use a RNN as an "encoder"
    to get a single, fixed size vector representation of a sequence
    (e.g. by taking element-wise max of hidden states).
    Here, we're using the RNN as an "encoder" but we're not taking max;
    we're just returning all the hidden states. The terminology "encoder"
    still applies because we're getting a different "encoding" of each
    position in the sequence, and we'll use the encodings downstream in the model.

    This code uses a bidirectional GRU, but you could experiment with other types of RNN.
    """

    def __init__(self, hidden_size, keep_prob):
        """
        Inputs:
          hidden_size: int. Hidden size of the RNN
          keep_prob: Tensor containing a single scalar that is the keep probability (for dropout)
        """
        self.hidden_size = hidden_size
        self.keep_prob = keep_prob
        # self.rnn_cell_fw = tf.contrib.rnn.LayerNormBasicLSTMCell(self.hidden_size, layer_norm=True)
        self.rnn_cell_fw = rnn_cell.LSTMCell(self.hidden_size)
        self.rnn_cell_fw = DropoutWrapper(self.rnn_cell_fw, input_keep_prob=self.keep_prob)
        # self.rnn_cell_bw = tf.contrib.rnn.LayerNormBasicLSTMCell(self.hidden_size, layer_norm=True)
        self.rnn_cell_bw = rnn_cell.LSTMCell(self.hidden_size)
        self.rnn_cell_bw = DropoutWrapper(self.rnn_cell_bw, input_keep_prob=self.keep_prob)

    def build_graph(self, inputs, masks, name="RNNEncoder"):
        """
        Inputs:
          inputs: Tensor shape (batch_size, seq_len, input_size)
          masks: Tensor shape (batch_size, seq_len).
            Has 1s where there is real input, 0s where there's padding.
            This is used to make sure tf.nn.bidirectional_dynamic_rnn doesn't iterate through masked steps.

        Returns:
          out: Tensor shape (batch_size, seq_len, hidden_size*2).
            This is all hidden states (fw and bw hidden states are concatenated).
        """
        with vs.variable_scope(name):
            input_lens = tf.reduce_sum(masks, reduction_indices=1) # shape (batch_size)

            # Note: fw_out and bw_out are the hidden states for every timestep.
            # Each is shape (batch_size, seq_len, hidden_size).
            (fw_out, bw_out), _ = tf.nn.bidirectional_dynamic_rnn(self.rnn_cell_fw, self.rnn_cell_bw, inputs, input_lens, dtype=tf.float32)

            # Concatenate the forward and backward hidden states
            out = tf.concat([fw_out, bw_out], 2)

            # Apply dropout
            out = tf.nn.dropout(out, self.keep_prob)

            return out


class SimpleSoftmaxLayer(object):
    """
    Module to take set of hidden states, (e.g. one for each context location),
    and return probability distribution over those states.
    """

    def __init__(self, l2_lambda):
        self.l2_lambda = l2_lambda

    def build_graph(self, inputs, masks):
        """
        Applies one linear downprojection layer, then softmax.

        Inputs:
          inputs: Tensor shape (batch_size, seq_len, hidden_size)
          masks: Tensor shape (batch_size, seq_len)
            Has 1s where there is real input, 0s where there's padding.

        Outputs:
          logits: Tensor shape (batch_size, seq_len)
            logits is the result of the downprojection layer, but it has -1e30
            (i.e. very large negative number) in the padded locations
          prob_dist: Tensor shape (batch_size, seq_len)
            The result of taking softmax over logits.
            This should have 0 in the padded locations, and the rest should sum to 1.
        """
        with vs.variable_scope("SimpleSoftmaxLayer"):

            # Linear downprojection layer
            # logits = tf.layers.dense(inputs,
            #                          units=1,
            #                          activation=None,
            #                          kernel_initializer=initializer(),
            #                          kernel_regularizer=tf.contrib.layers.l2_regularizer(self.l2_lambda))
            logits = conv1d(inputs,
                            output_size=1,
                            l2_lambda=self.l2_lambda)
            logits = tf.squeeze(logits, axis=[2]) # shape (batch_size, seq_len)

            # Take softmax over sequence
            masked_logits, prob_dist = masked_softmax(logits, masks, 1)

            return masked_logits, prob_dist


class BasicAttn(object):
    """Module for basic attention.

    Note: in this module we use the terminology of "keys" and "values" (see lectures).
    In the terminology of "X attends to Y", "keys attend to values".

    In the baseline model, the keys are the context hidden states
    and the values are the question hidden states.

    We choose to use general terminology of keys and values in this module
    (rather than context and question) to avoid confusion if you reuse this
    module with other inputs.
    """

    def __init__(self, keep_prob, key_vec_size, value_vec_size):
        """
        Inputs:
          keep_prob: tensor containing a single scalar that is the keep probability (for dropout)
          key_vec_size: size of the key vectors. int
          value_vec_size: size of the value vectors. int
        """
        self.keep_prob = keep_prob
        self.key_vec_size = key_vec_size
        self.value_vec_size = value_vec_size

    def build_graph(self, values, values_mask, keys):
        """
        Keys attend to values.
        For each key, return an attention distribution and an attention output vector.

        Inputs:
          values: Tensor shape (batch_size, num_values, value_vec_size).
          values_mask: Tensor shape (batch_size, num_values).
            1s where there's real input, 0s where there's padding
          keys: Tensor shape (batch_size, num_keys, value_vec_size)

        Outputs:
          attn_dist: Tensor shape (batch_size, num_keys, num_values).
            For each key, the distribution should sum to 1,
            and should be 0 in the value locations that correspond to padding.
          output: Tensor shape (batch_size, num_keys, hidden_size).
            This is the attention output; the weighted sum of the values
            (using the attention distribution as weights).
        """
        with vs.variable_scope("BasicAttn"):

            # Calculate attention distribution
            values_t = tf.transpose(values, perm=[0, 2, 1]) # (batch_size, value_vec_size, num_values)
            attn_logits = tf.matmul(keys, values_t) # shape (batch_size, num_keys, num_values)
            attn_logits_mask = tf.expand_dims(values_mask, 1) # shape (batch_size, 1, num_values)
            _, attn_dist = masked_softmax(attn_logits, attn_logits_mask, 2) # shape (batch_size, num_keys, num_values). take softmax over values

            # Use attention distribution to take weighted sum of values
            output = tf.matmul(attn_dist, values) # shape (batch_size, num_keys, value_vec_size)

            # Apply dropout
            output = tf.nn.dropout(output, self.keep_prob)

            return attn_dist, output


def masked_softmax(logits, mask, dim):
    """
    Takes masked softmax over given dimension of logits.

    Inputs:
      logits: Numpy array. We want to take softmax over dimension dim.
      mask: Numpy array of same shape as logits.
        Has 1s where there's real data in logits, 0 where there's padding
      dim: int. dimension over which to take softmax

    Returns:
      masked_logits: Numpy array same shape as logits.
        This is the same as logits, but with 1e30 subtracted
        (i.e. very large negative number) in the padding locations.
      prob_dist: Numpy array same shape as logits.
        The result of taking softmax over masked_logits in given dimension.
        Should be 0 in padding locations.
        Should sum to 1 over given dimension.
    """
    exp_mask = (1 - tf.cast(mask, 'float')) * (-1e30) # -large where there's padding, 0 elsewhere
    masked_logits = tf.add(logits, exp_mask) # where there's padding, set logits to -large
    prob_dist = tf.nn.softmax(masked_logits, dim)
    return masked_logits, prob_dist


def get_similarity_matrix(context,
                        query,
                        l2_lambda,
                        scope="similarity_matrix",
                        weight_initializer=None,
                        use_bias=False,
                        bias_initializer=None):
    """内存利用更少的similarity_matrix 的计算，思路来自https://github.com/chrischute/squad

    similarity matrix 来源于 https://arxiv.org/abs/1611.01603

    Args:
        context: Tensor shape (batch_size, c_len, vec_size)
        query: Tensor shape (batch_size, q_len, vec_size)
        l2_lambda: Float. lambda of l2 regularizer
        scope: `string` or `VariableScope`: the scope to open.
        weight_initializer: initializer of weights
        bias_initializer: initializer of bias.
        use_bias: Boolean, whether the similarity uses a bias.

    Returns:
        similarity matrix: Tensor shape (batch_size, context_len, query_len)
    """
    _, c_len, vec_size = context.shape.as_list()
    _, q_len, q_vec_size = query.shape.as_list()
    assert vec_size == q_vec_size, "context and query must have the same vector size"

    with tf.variable_scope(scope):
        # shape (batch_size, c_len, 1)
        c = tf.layers.dense(inputs=context,
                            units=1,
                            activation=None,
                            use_bias=False,
                            kernel_initializer=weight_initializer,
                            kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
        # shape (batch_size, q_len, 1)
        q = tf.layers.dense(inputs=query,
                            units=1,
                            activation=None,
                            use_bias=False,
                            kernel_initializer=weight_initializer,
                            kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
        # shape (batch_size, c_len, q_len)
        q = tf.tile(tf.transpose(q, [0, 2, 1]), (1, c_len, 1))
        # 为什么可以这么做，仔细比较同一下标位置的元素即可，在note/QANet.md中有提到这个
        # shape (batch_size, c_len, q_len)
        cq_weight = tf.get_variable("cq_weight",
                                    shape=(1, 1, vec_size),
                                    initializer=weight_initializer,
                                    regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
        cq = tf.matmul(context * cq_weight, tf.transpose(query, [0, 2, 1]))

        if use_bias:
            bias = tf.get_variable("similarity_bias",
                                   shape=(q_len,),
                                   dtype=context.dtype,
                                   initializer=bias_initializer)
            return tf.nn.bias_add(c + q + cq, bias)
        else:
            return c + q + cq


class BiDirectionalAttn(object):
    """Module for bi-directional attention.

    和BasicAttn不同，除了 "keys attend to values", 还有另一个方向 “values attend to keys"
    本层的输入是context和question的hidden states，输出的是 question-aware vector representation
    """
    def __init__(self, keep_prob, l2_lambda):
        self.keep_prob = keep_prob
        self.l2_lambda = l2_lambda

    def build_graph(self, keys, values, keys_mask, values_mask):
        """
        Keys attend to values and values attend to keys.

        For each key, return one attention distribution and one attention output vector (summed values)
        For all values, return one attention distribution and one attention output vector (summed keys)
        来自 [Bidirectional Attention Flow for Machine Comprehension](https://arxiv.org/pdf/1611.01603.pdf)，
        其中Context对应Key, Query对应Value

        Key2Value: which value(which query word) are most relevant to one of the key(context word). Weighted sum of the values


        Value2key: which key(context word) have the closest similarity
        to all values(the query). Weighted sum of the keys(context hidden states)
                这个概率分布对于回答query很有帮助，因为指出了对于query上下文中哪个word的重要程度。

        Inputs:
          keys: Tensor shape (batch_size, num_keys, value_vec_size)
          values: Tensor shape (batch_size, num_values, value_vec_size).
          keys_mask: Tensor shape (batch_size, num_keys).
            1s where there's real input, 0s where there's padding
          values_mask: Tensor shape (batch_size, num_values).
            1s where there's real input, 0s where there's padding

        Outputs:
            key-2-value-attn-dist: Tensor shape (batch_size, num_keys, num_values).
            value-2-key-attn-dist: Tensor shape (batch_size, num_keys).
            key2value attn output: Tensor shape (batch_size, num_keys, value_vec_size).
            value2key attn output: Tensor shape (batch_size, 1, key_vec_size).
        """
        key_vec_size = keys.shape.as_list()[-1]
        value_vec_size = values.shape.as_list()[-1]
        with vs.variable_scope("BidirectionalAttn"):
            assert value_vec_size == key_vec_size, "value and key must have the same vector size"

            num_values = tf.shape(values)[1] # num_values
            num_keys = tf.shape(keys)[1] # num_keys

            # 计算 Similarity Matrix
            similarity_score = get_similarity_matrix(context=keys,
                                                     query=values,
                                                     l2_lambda=self.l2_lambda,
                                                     weight_initializer=initializer())

            # masked logits
            keys_mask_aug = tf.tile(tf.expand_dims(keys_mask, axis=2), [1,1,num_values]) # batch_size, num_keys, num_values
            values_mask_aug = tf.tile(tf.expand_dims(values_mask, axis=1), [1, num_keys, 1]) #  batch_size, num_keys, num_values
            similarity_mask = tf.cast(keys_mask_aug, tf.bool) & tf.cast(values_mask_aug, tf.bool)  # shape (batch_size, num_keys, num_values)
            exp_mask = (1.0 - tf.cast(similarity_mask, tf.float32)) * (-1e30)
            masked_logits = tf.add(similarity_score, exp_mask) # shape (batch_size, num_keys, num_values)

            # Key-to-Value attention.
            # For each key, get the softmax distribution over the values.
            k2v_attn_dist = tf.nn.softmax(masked_logits, dim=-1) # shape (batch_size, num_keys, num_values)
            k2v_output = tf.matmul(k2v_attn_dist, values) # shape (batch_size, num_keys, value_vec_size)

            # Value-to-key attention. 对于values(query)的keys(context)的权重和
            # Find the max value for each key.(each row of similarity matrix)
            max_score_each_key = tf.reduce_max(masked_logits, axis=-1) # shape(batch_size, num_keys)
            # Get an attention distribution over the keys.
            _, v2k_attn_dist = masked_softmax(max_score_each_key, mask=keys_mask, dim=-1) # shape(batch_size, num_keys)
            # Take the weighted sum of the keys as output.
            v2k_output = tf.matmul(tf.expand_dims(v2k_attn_dist, axis=1), keys) # shape(batch_size, 1, key_vec_size)

            # Apply dropout
            k2v_output = tf.nn.dropout(k2v_output, self.keep_prob)
            v2k_output = tf.nn.dropout(v2k_output, self.keep_prob)

            return k2v_attn_dist, v2k_attn_dist, k2v_output, v2k_output


class DynamicCoAttention(object):
    """Dynamic CoAttention

    From https://arxiv.org/abs/1611.01604
    实现和记号都来自于QANet https://arxiv.org/abs/1804.09541
    """
    def __init__(self, keep_prob, l2_lambda):
        self.keep_prob = keep_prob
        self.l2_lambda = l2_lambda

    def build_graph(self, keys, values, keys_mask, values_mask):
        """Context-Query Attention Layer

        其中Context对应Key, Query对应Value

        Args:
            keys: Tensor shape (batch_size, num_keys, value_vec_size)
            values: Tensor shape (batch_size, num_values, value_vec_size).
            keys_mask: Tensor shape (batch_size, num_keys).
               1s where there's real input, 0s where there's padding
            values_mask: Tensor shape (batch_size, num_values).
               1s where there's real input, 0s where there's padding

        Returns:
            key-2-value-attn-dist: Tensor shape (batch_size, num_keys, num_values).
            value-2-key-attn-dist: Tensor shape (batch_size, num_keys, num_keys).
            key2value attn output: Tensor shape (batch_size, num_keys, value_vec_size).
            value2key attn output: Tensor shape (batch_size, num_keys, key_vec_size).
        """
        key_vec_size = keys.shape.as_list()[-1]
        value_vec_size = values.shape.as_list()[-1]
        with vs.variable_scope("DynamicCoAttention"):
            assert value_vec_size == key_vec_size, "value and key must have the same vector size"

            # 计算 Similarity Matrix
            similarity_score = get_similarity_matrix(context=keys,
                                                     query=values,
                                                     l2_lambda=self.l2_lambda,
                                                     weight_initializer=initializer())

            # masked similarity softmax prob
            keys_mask = tf.expand_dims(keys_mask, 2)  # shape (batch_size, num_keys, 1)
            values_mask = tf.expand_dims(values_mask, 1)  # shape (batch_size, 1, num_values)
            _, s_row = masked_softmax(similarity_score, values_mask, dim=2)
            _, s_col = masked_softmax(similarity_score, keys_mask, dim=1)

            # k2v(c2q) attention
            k2v_dist = s_row
            k2v_attn = tf.matmul(k2v_dist, values)

            # v2k(q2c) attention
            v2k_dist = tf.matmul(s_row, tf.transpose(s_col, [0,2,1]))
            v2k_attn = tf.matmul(v2k_dist, keys)

            # Apply dropout
            k2v_output = tf.nn.dropout(k2v_attn, self.keep_prob)
            v2k_output = tf.nn.dropout(v2k_attn, self.keep_prob)

            return k2v_dist, v2k_dist, k2v_output, v2k_output


class ScaledDotProductAttention(object):
    """Scaled Dot-Product Attention

    输入一个query和key-value pairs, 输出values的权重和，其中每个value的权重计算
    来自于对应的key和query的内积。具体公式为（Q：queries， keys:K, values:V):
    Attention$(Q, K, V)$ = softmax($\frac{QK^T}{\sqrt{d_k}}) V$

    来自 [Attention Is All You Need](https://arxiv.org/abs/1706.03762) 3.2.1
    缩放dot-product，可能避免随着$d_k$的变大（假定Q，K独立)，softmax后的分布过于陡峭，导致梯度消失，很难训练
    """
    def __init__(self, keep_prob):
        """
        Inputs:
          keep_prob: tensor containing a single scalar that is the keep probability (for dropout)
        """
        self.keep_prob = keep_prob

    def build_graph(self, queries, keys, values, values_mask, use_bias=True) :
        """构建一个Attention function： 将一个query和key-value pairs作为输入，输出values的权重和

        实际代码是矩阵化的，同时对多个Head的所有的queries进行计算。

        Inputs:
            queries: Tensor shape (batch_size, num_heads, num_queries, query_vec_size)
            keys: Tensor shape (batch_size, num_heads, num_keys, key_vec_size)
            values: Tensor shape (batch_size, num_heads, num_values, value_vec_size)
            values_mask: Tensor shape (batch_size, num_values)

        Outputs:
            attn_dist: Tensor shape (batch_size, num_heads, num_queries, num_keys)
              每个query对应的分布和为1,value padding的位置值为0
            attn_output: Tensor shape (batch_size, num_heads, num_queries, value_vec_size)
        """
        num_values = values.shape.as_list()[-2]
        num_keys = keys.shape.as_list()[-2]
        assert num_values == num_keys, "num_values != num_keys, num_values:{0}, num_keys:{1}".format(num_values, num_keys)

        with vs.variable_scope("ScaledDotProductAttn"):
            keys_vector_size = tf.to_float(tf.shape(keys)[-1])
            logits = tf.matmul(queries, keys, transpose_b=True) * tf.rsqrt(keys_vector_size)
            if use_bias:
                bias = tf.get_variable("bias",
                                    logits.shape[-1],
                                    regularizer=regularizer,
                                    initializer=tf.zeros_initializer())
                logits += bias

            logits_mask = tf.reshape(values_mask, [-1,1,1,num_values])
            _, attn_dist = masked_softmax(logits, logits_mask, -1) # shape (batch_size, num_heads, num_queries, num_keys)
            attn_output = tf.matmul(attn_dist, values) # shape (batch_size, num_heads, num_queries, value_vec_size)

            # dropout
            attn_output = tf.nn.dropout(attn_output, self.keep_prob)
            return attn_dist, attn_output


def split_head(x, num_heads):
    """将x最后一维映射到 x.shape[-1]/num_heads的子空间, 并transpose

    Args:
        x: Tensor shape (batch_size, length, vec_size)
        num_heads: int. 需要split的heads数量
    Returns:
        output: Tensor shape (batch_size , num_heads, length, vec_size/num_heads)
    """
    _, length, vec_size = x.shape.as_list()
    if vec_size % num_heads != 0:
        raise ValueError("channel_size:{0} 不能分成 num_heads:{1}份".format(vec_size, num_heads))
    heads = tf.reshape(x, shape=[-1, length, num_heads, vec_size//num_heads])
    return tf.transpose(heads, perm=[0, 2, 1, 3]) # shape (batch_size, num_heads, length, channel_size//num_heads)


class MultiHeadAttn(object):
    """Multi-Head Attention

    将queries, keys和values使用不同的权重线性映射h次，分别映射到维度为（$d_k, d_k, d_v$) 的空间上;
    然后使用h个attention function并行得出 h 个 $d_v$ 维的输出;
    最后连接这些输出并线性映射到 $d_{model}$维上

    MultiHead(Q, K, V) = Concat(head_1, head_2, ..., head_h) W^o
    head_i = ScaledDotProductAttn($QW_i^Q, K W_i^K, V W_i^V$)
    """
    def __init__(self, head_vec_size, num_heads, keep_prob_attn, l2_lambda):
        """
        Inputs:
          head_vec_size: size of the each head. int, output
          num_heads: number of heads. int
          keep_prob_attn: scalar. the keep probability for dropout
          l2_lambda: Float. lambda of l2 regularizer
        """
        self.head_vec_size = head_vec_size
        self.keep_prob_attn = keep_prob_attn
        self.num_heads = num_heads
        self.model_vec_size = head_vec_size * num_heads
        self.l2_lambda = l2_lambda

    def build_graph(self,
                    queries,
                    keys,
                    values,
                    values_mask,
                    use_bias=True,
                    name="MultiHeadAttn") :
        """将输入分成多个heads，然后得到scaled dot product attention,
         最后再映射到$head_vec_size * num_heads$维的空间上

        Note: 输入需要满足 num_keys = num_values, query_vec_size = key_vec_size

        Inputs:
            queries: Tensor shape (batch_size, num_queries, query_vec_size)
            keys: Tensor shape (batch_size, num_keys, key_vec_size)
            values: Tensor shape (batch_size, num_values, value_vec_size)
            values_mask: Tensor shape (batch_size, num_values)
            use_bias: True: use bias in scaled dot-product attention.
            name: name of variable_scope

        Outputs:
            attn_dist: Tensor shape (batch_size, num_heads,  num_queries, num_keys)
            attn_output: Tensor shape (batch_size, num_queries, head_vec_size * num_heads)
        """
        with vs.variable_scope(name):
            # 用得到的shape
            batch_size, num_queries, query_vec_size = queries.shape.as_list()
            _, num_keys, key_vec_size = keys.shape.as_list()
            _, num_values, value_vec_size = values.shape.as_list()

            # 验证输入是否满足shape的约束
            if num_keys != num_values:
                raise ValueError("num_keys: {0} 必须等于 num_values: {1}".format(num_keys, num_values))
            if query_vec_size != key_vec_size:
                raise ValueError("query vec size: {0} 必须等于 key vec size: {1}".format(query_vec_size, key_vec_size))

            # split head
            q = conv1d(queries,
                       output_size=self.model_vec_size,
                       l2_lambda=self.l2_lambda,
                       name="query")
            k = conv1d(keys,
                       output_size=self.model_vec_size,
                       l2_lambda=self.l2_lambda,
                       name="key")
            v = conv1d(values,
                       output_size=self.model_vec_size,
                       l2_lambda=self.l2_lambda,
                       name="value")
            q = split_head(q, self.num_heads)
            k = split_head(k, self.num_heads)
            v = split_head(v, self.num_heads)

            # 获得scaled dot product attention
            attn = ScaledDotProductAttention(self.keep_prob_attn)
            attn_dist, output = attn.build_graph(q, k, v, values_mask, use_bias) # shape (batch_size, num_heads, num_queries, head_vec_size)

            # combine head
            output = tf.transpose(output, perm=[0, 2, 1, 3]) # shape (batch_size, num_queries, num_heads, value_vec_size//num_heads)
            output = tf.reshape(output, shape=[-1, num_queries, self.model_vec_size]) # shape (batch_size, num_queries, value_vec_size)

            # linear
            # attn_output = tf.layers.dense(output,
            #                               units=self.model_vec_size,
            #                               name="linear",
            #                               kernel_initializer=initializer(),
            #                               kernel_regularizer=tf.contrib.layers.l2_regularizer(self.l2_lambda))

            # Apply dropout
            attn_output = tf.nn.dropout(output, self.keep_prob_attn)
            return attn_dist, attn_output


def residual_block_with_layer_dropout(inputs,
                                      residual,
                                      layer=1,
                                      num_layers=1,
                                      last_layer_survival_prob=1.0):
    """A residual block with identity mapping shortcut and layer dropout.

    Return inputs if the layer's residual output is randomly dropped by a linear decay layer survival probability.
    layer dropout - https://arxiv.org/abs/1603.09382.pdf
    residual block - https://arxiv.org/abs/1512.03385.pdf

    Args:
        inputs: Tensor.
        residual: Tensor, same shape with inputs
        layer: int. `l` which layer, [0-num_layers]
        num_layers: int. L the total number of layers
        last_layer_survival_prob: the survival probability of the last layer.

    Returns:
        outputs: Tensor. same shape with inputs
    """
    # linear decay prob from 1.0 -> last_layer_survival_prob with layer from 0 -> num_layers
    assert num_layers > 0
    survival_probability = 1.0 - float(layer) / float(num_layers) * ( 1.0 - last_layer_survival_prob)
    # layer dropout
    return tf.cond(pred=tf.random_uniform(shape=()) > survival_probability,
                   true_fn=lambda: inputs,
                   false_fn=lambda: inputs + residual)


def layer_normalization(summed_inputs,
                        epsilon=1e-6,
                        name="layer_normalization",
                        l2_lambda=None,
                        gain_initializer=tf.ones_initializer,
                        bias_initializer=tf.zeros_initializer):
    """对 summed_inputs 进行标准化， 具体参见https://arxiv.org/pdf/1607.06450.pdf

    Args:
        summed_inputs: tensor, 每一层的输入经过权重矩阵线性映射后的权重和
        epsilon: 避免被除0
        name: 所处的variable scope
        l2_lambda: Float. lambda of l2 regularizer; if None then no regularizer
        gain_initializer: gain parameter的 initializer
        bias_initializer: bias的initializer
    Returns:
        进行标准化后的Tensor
    """
    dim = summed_inputs.shape.as_list()[-1]
    with vs.variable_scope(name):
        gain_variable = tf.get_variable("gain",
                                        shape=dim,
                                        initializer=gain_initializer,
                                        regularizer=tf.contrib.layers.l2_regularizer(l2_lambda) if l2_lambda else None)
        bias_variable = tf.get_variable("bias",
                                        shape=dim,
                                        initializer=bias_initializer,
                                        regularizer=tf.contrib.layers.l2_regularizer(l2_lambda) if l2_lambda else None)

        mean = tf.reduce_mean(summed_inputs, axis=-1, keep_dims=True)
        variance = tf.reduce_mean(tf.square(summed_inputs - mean), axis=-1, keep_dims=True)
        norm_summed_inputs = (summed_inputs - mean) * tf.rsqrt(variance + epsilon)
        return norm_summed_inputs * gain_variable + bias_variable


def positional_encoding(inputs,
                        min_timescale=1.0,
                        max_timescale=1.0e4):
    """将inputs加上位置编码

    Transfer Model只使用了Attention机制，需要用某种方式表示序列中token的
    绝对位置和相对位置，所以我们对每个维度，以位置作为参数，偶数维度输出正弦，
    奇数维度输出余弦，波长为维度i的函数,从2pi- 10000 * 2pi

    [Attention is All You Need](https://arxiv.org/abs/1706.03762)提到:
    采用正弦波来编码，可以使得 PE_{pos+k} 表示为 PE_{pos}的线性关系, 准确的说
    PE_{pos+k, 2i} 是 PE_{pos, 2i}, PE_{pos, 2i+1}的线性组合，这一点也许对于
    相对位置有帮助

    实现来自 tensor2tensor.layers.common_attention.get_timing_signal_1d

    Args:
        inputs: Tensor shape (batch_size, num_values, vector_size)
        min_timescale:
        max_timescale:
    Returns:
        output: Tensor shape (1, num_values, vector_size) 增加了位置编码的输出
    """
    length, channels = inputs.shape.as_list()[-2:]
    position = tf.to_float(tf.range(length))
    num_timescales = channels // 2
    log_timescale_increment = (
            tf.log(float(max_timescale) / float(min_timescale)) /
            (tf.to_float(num_timescales) - 1))
    inv_timescales = min_timescale * tf.exp(
        tf.to_float(tf.range(num_timescales)) * -log_timescale_increment)
    scaled_time = tf.expand_dims(position, 1) * tf.expand_dims(inv_timescales, 0)
    signal = tf.concat([tf.sin(scaled_time), tf.cos(scaled_time)], axis=1)
    signal = tf.pad(signal, [[0, 0], [0, tf.mod(channels, 2)]])
    signal = tf.reshape(signal, [1, length, channels])
    # 加上signal
    return inputs + signal


def conv1d(inputs,
           output_size,
           kernel_size=1,
           l2_lambda=3e-7,
           padding="SAME",
           activation=None,
           use_bias=True,
           name="conv1d"):
    """standard convolutional layer.

    Args:
        inputs: Tensor shape (batch_size, seq_len, vec_size)
        output_size: Int. the last dimension of output.
        kernel_size: An integer or tuple/list of a single integer, specifying the
          length of the 1D convolution window.
        l2_lambda: Float. lambda of l2 regularizer
        name: String. name of the scope
        use_bias: Boolean, whether the layer uses a bias.

    Returns:

    """
    input_size = inputs.shape.as_list()[-1]
    with tf.variable_scope(name):
        filters = tf.get_variable("filter",
                                  shape=(kernel_size, input_size, output_size),
                                  initializer = initializer() if activation is None else initializer_relu(),
                                  regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
        outputs = tf.nn.conv1d(inputs,
                               filters=filters,
                               stride=1,
                               padding=padding)
        if use_bias:
            bias = tf.get_variable("bias",
                                   shape=(output_size,),
                                   initializer=tf.zeros_initializer)
            outputs += bias
        if activation is not None:
            return activation(outputs)
        else:
            return outputs



def separable_conv1d(inputs,
                     filters,
                     kernel_size,
                     l2_lambda,
                     use_bias=True,
                     name="separable_conv1d"):
    """Separable convolutions

    Args:
        inputs: Tensor shape(batch_size, seq_len, vec_size)
        filters: Int. the number of filters.
        kernel_size: An integer or tuple/list of a single integer, specifying the
          length of the 1D convolution window.
        l2_lambda: Float. lambda of l2 regularizer
        name: String. name of the scope

    Returns:
        outputs: Tensor shape(batch_size, seq_len, filters)
    """
    vec_size = inputs.shape.as_list()[-1]
    with tf.variable_scope(name):
        # 将inputs的seq_len当作H，在之后添加一个维度当作W，转成rank为4的tensor,以满足default data_format NHWC
        inputs = tf.expand_dims(inputs, axis=2)
        # depthwise 4-D `Tensor` with shape `[filter_height, filter_width, in_channels, channel_multiplier]`.
        depthwise_filter = tf.get_variable("depthwise_filter",
                                           shape=(kernel_size, 1, vec_size, 1),
                                           initializer=initializer_relu(),
                                           regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))

        # pointwise_filter: 4-D `Tensor` with shape `[1, 1, channel_multiplier * in_channels, out_channels]`.
        pointwise_filter = tf.get_variable("pointwise_filter",
                                           shape=(1,1,vec_size,filters),
                                           initializer=initializer_relu(),
                                           regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
        outputs = tf.nn.separable_conv2d(inputs, depthwise_filter, pointwise_filter, (1,1,1,1), "SAME")
        if use_bias:
            bias = tf.get_variable("bias",
                                   shape=outputs.shape[-1],
                                   initializer=tf.zeros_initializer)
            outputs += bias

        # 之前添加的W移除
        return tf.squeeze(outputs, axis=2)


class TransformerEncoderBlock:
    """结构来自于[QANet](https://arxiv.org/abs/1804.09541)中的Encoder Block

     基本上是[Transformer](https://arxiv.org/abs/1706.03762)中的block的encoder + separable conv layer x N
     """

    def __init__(self,
                 model_size,
                 ffn_size,
                 num_conv_layer,
                 num_heads,
                 keep_prob,
                 l2_lambda,
                 kernel_size,
                 last_layer_survival_prob,
                 total_layer):
        """ Initializer

        Args:
            model_size: int. 论文中的d_model, model size
            ffn_size: int 中间层的size，feedforward由2个线性映射+上中间的ReLU组成，
            num_conv_layer: int. number of the convolutional layers
            num_heads: int. number of heads.
            keep_prob: Tensor 简单起见，就用了一个keep_prob用于encoder内部不同Attention和Feed Forward的 dropout
            l2_lambda: Float. lambda of l2 regularizer
            kernel_size: An integer or tuple/list of a single integer, specifying the
              length of the 1D convolution window.
            last_layer_survival_prob: the survival probability of the last layer.
            total_layer: int. total number of layers
        """
        self.model_size = model_size
        self.num_heads = num_heads
        self.num_conv_layer = num_conv_layer
        self.ffn_size = ffn_size
        self.keep_prob = keep_prob
        self.l2_lambda = l2_lambda
        self.kernel_size = kernel_size
        self.start_layer = 0
        self.total_layer = total_layer
        self.last_layer_survival_prob = last_layer_survival_prob
        if model_size % num_heads != 0:
            raise ValueError("model_size:{0} 不是 num_heads:{1}的倍数".format(model_size, num_heads))

    def _build_convolution_layers(self, inputs):
        """build convolution layers with the number of self.num_conv_layer

        Args:
            inputs: Tensor shape (batch_size, seq_len, input_size)

        Returns:
            outputs: Tensor shape (batch_size, seq_len, input_size)
        """
        input_vec_size = inputs.shape.as_list()[-1]
        x = inputs
        for i in range(self.num_conv_layer):
            conv_inputs_ln = layer_normalization(x, self.l2_lambda, name="conv_{0}_layer_norm".format(i+1))
            #Try dropout every 2 conv layer?
            if i % 2 == 0:
                conv_inputs_ln = tf.nn.dropout(conv_inputs_ln, self.keep_prob)
            conv_outputs = separable_conv1d(conv_inputs_ln,
                                            filters=input_vec_size,
                                            kernel_size=self.kernel_size,
                                            l2_lambda=self.l2_lambda,
                                            name="conv_{0}".format(i + 1))
            x = residual_block_with_layer_dropout(inputs=x,
                                                  residual=conv_outputs,
                                                  layer=self.start_layer,
                                                  num_layers=self.total_layer,
                                                  last_layer_survival_prob=self.last_layer_survival_prob)
            self.start_layer += 1
        return x

    def _build_attention_layer(self, inputs, masks, use_bias=True):
        """ layer_norm + attention + dropout + residual + layer dropout

        Args:
            inputs: Tensor shape (batch_size, seq_len, input_size)
            masks: Tensor shape (batch_size, seq_len).
              Has 1s where there is real input, 0s where there's padding.
        Returns:
            outputs: Tensor shape (batch_size, seq_len, input_size)
        """
        attn_inputs = layer_normalization(inputs, self.l2_lambda, name="attn_layer_norm")
        attn_inputs = tf.nn.dropout(attn_inputs, self.keep_prob)
        attn = MultiHeadAttn(head_vec_size=self.model_size//self.num_heads,
                             num_heads=self.num_heads,
                             keep_prob_attn=self.keep_prob,
                             l2_lambda=self.l2_lambda)
        _, attn_output = attn.build_graph(attn_inputs, attn_inputs, attn_inputs, masks, use_bias=use_bias)
        # from layers import multihead_attention
        # attn_output = multihead_attention(attn_inputs, self.FLAGS.encoder_model_size, self.num_heads)
        # residual block
        outputs = residual_block_with_layer_dropout(inputs=inputs,
                                                    residual=attn_output,
                                                    layer=self.start_layer,
                                                    num_layers=self.total_layer,
                                                    last_layer_survival_prob=self.last_layer_survival_prob)
        self.start_layer += 1
        return outputs

    def _build_feedforward_layer(self, inputs):
        """ layer_norm + dense + dropout  + residual + layer_dropout

        Args:
            inputs: Tensor shape (batch_size, seq_len, input_size)
        Returns:
            outputs: Tensor shape (batch_size, seq_len, input_size)
        """
        ffn_inputs = layer_normalization(inputs, self.l2_lambda, name="ffn_layer_norm")
        ffn_inputs = tf.nn.dropout(ffn_inputs, self.keep_prob)
        # non_linear_outputs = tf.layers.dense(ffn_inputs,
        #                                      units=self.model_size,
        #                                      activation=tf.nn.relu,
        #                                      kernel_initializer=initializer_relu(),
        #                                      kernel_regularizer=tf.contrib.layers.l2_regularizer(self.l2_lambda))
        non_linear_outputs = conv1d(ffn_inputs,
                                    output_size=self.model_size,
                                    activation=tf.nn.relu,
                                    name="ffw_non_linear")
        linear_outputs = conv1d(non_linear_outputs,
                                output_size=self.model_size,
                                name="ffw_linear")
        linear_outputs = tf.nn.dropout(linear_outputs, self.keep_prob)


        # residual block
        outputs = residual_block_with_layer_dropout(inputs=inputs,
                                                    residual=linear_outputs,
                                                    layer=self.start_layer,
                                                    num_layers=self.total_layer,
                                                    last_layer_survival_prob=self.last_layer_survival_prob)
        self.start_layer += 1
        return outputs

    def build_graph(self,
                    inputs,
                    masks,
                    start_layer,
                    name="transformer_encoder",
                    use_attn_bias=True,
                    reuse=None):
        """
        Args:
            inputs: Tensor shape (batch_size, seq_len, input_size)
            masks: Tensor shape (batch_size, seq_len).
              Has 1s where there is real input, 0s where there's padding.
            name: String. the name of scope
            use_attn_bias: True: use bias in attention layer
            reuse: `True`, None, or tf.AUTO_REUSE

        Returns:
          out: Tensor shape (batch_size, seq_len, input_size).
        """
        self.start_layer = start_layer
        with vs.variable_scope(name, reuse=reuse):
            # 由于使用了residual block，需要保证inputs的size和model size一样
            input_vec_size = inputs.shape.as_list()[-1]
            if input_vec_size != self.model_size:
                raise ValueError("input_vec_size {0} != model_size:{1}".format(input_vec_size, self.model_size))

            # add positional embedding
            conv_inputs = positional_encoding(inputs)

            # separable conv1d
            conv_outputs = self._build_convolution_layers(conv_inputs)

            # multiple heads attention
            attn_outputs = self._build_attention_layer(conv_outputs, masks, use_bias=use_attn_bias)

            # feedforward
            outputs = self._build_feedforward_layer(attn_outputs)
            return outputs
            # return self._build_feedforward_layer(attn_outputs)


class StackedTransformerEncoderBlock(object):
    """堆叠的encoder block, share weights, 所以其实只用了一个Block，重复使用了N次"""
    def __init__(self,
                 num_stacked,
                 model_size,
                 ffn_size,
                 num_conv_layer,
                 num_heads,
                 keep_prob,
                 l2_lambda,
                 kernel_size,
                 last_layer_survival_prob,
                 name):
        """ Initializer.

        Args:
            num_stacked:  int. 堆叠的数目，表示输入在encoder block被forward的次数
            model_size: int. 论文中的d_model, model size
            ffn_size: int 中间层的size，feedforward由2个线性映射+上中间的ReLU组成，
            num_conv_layer: int. number of the convolutional layers
            num_heads: int. number of heads.
            keep_prob: Tensor 简单起见，就用了一个keep_prob用于encoder内部不同Attention和Feed Forward的 dropout
            l2_lambda: Float. lambda of l2 regularizer
            kernel_size: An integer or tuple/list of a single integer, specifying the
              length of the 1D convolution window.
            last_layer_survival_prob: the survival probability of the last layer.
            name: String. the name of scope
        """
        assert num_stacked > 0, "num_stacked must be > 0"
        self.num_stacked = num_stacked
        self.model_size = model_size
        self.num_heads = num_heads
        self.num_conv_layer = num_conv_layer
        self.ffn_size = ffn_size
        self.keep_prob = keep_prob
        self.l2_lambda = l2_lambda
        self.kernel_size = kernel_size
        self.name = name
        self.last_layer_survival_prob = last_layer_survival_prob

    def build_graph(self, inputs, masks, use_attn_bias=True):
        """build the graph of stacked encoder

        Args:
            inputs: Tensor shape (batch_size, seq_len, input_size)
            masks: Tensor shape (batch_size, seq_len).
              Has 1s where there is real input, 0s where there's padding.
            use_attn_bias: True: use bias in attention layer
        Returns:
            outputs: Tensor shape (batch_size, seq_len, input_size).
        """
        with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
            total_layer = (self.num_conv_layer + 2) * self.num_stacked
            start_layer = 1
            encoder = TransformerEncoderBlock(model_size=self.model_size,
                                              ffn_size=self.ffn_size,
                                              num_conv_layer=self.num_conv_layer,
                                              num_heads=self.num_heads,
                                              keep_prob=self.keep_prob,
                                              l2_lambda=self.l2_lambda,
                                              kernel_size=self.kernel_size,
                                              last_layer_survival_prob=self.last_layer_survival_prob,
                                              total_layer=total_layer)
            for i in range(self.num_stacked):
                outputs = encoder.build_graph(inputs,
                                              masks,
                                              start_layer=start_layer,
                                              use_attn_bias=use_attn_bias,
                                              name="encoder_{0}".format(i+1))
                inputs = outputs
                start_layer += self.num_conv_layer + 2
            return outputs

def get_start_end_pos_with_max_length_constraint(start_dist, end_dist, max_length):
    """考虑最大长度约束后的start和end位置

    Args:
        start_dist: start 位置的概率分布 np.ndarray shape (batch_size, context_len)
        end_dist: 位置的概率分布 np.ndarray shape (batch_size, context_len)
        max_length: int. 满足约束 start <= end <= start+max_length. 0和负数表示无最大长度限制

    Returns:
        start_pos: np.ndarray shape (batch_size,)
        end_pos: np.ndarray shape (batch_size,)
    """
    # (start_pos, end_pos)的概率矩阵
    batch_size, context_len = start_dist.shape
    start_dist_aug = np.tile(np.expand_dims(start_dist, -1), [1,1,context_len])
    end_dist_aug = np.tile(np.expand_dims(end_dist, 1), [1,context_len,1])
    prob_dist = start_dist_aug * end_dist_aug # shape (batch_size, context_len, context_len)

    # 将无效的位置概率都设置为0
    max_length =  max_length if max_length > 0 else context_len
    lower = np.tril(np.ones((context_len, context_len)), -1)
    upper = np.triu(np.ones((context_len, context_len)), max_length+1)
    invalid_indices_mask = np.array(lower + upper, dtype=np.bool)
    invalid_indices_mask = np.tile(np.expand_dims(invalid_indices_mask, 0), [batch_size, 1, 1])
    np.putmask(prob_dist, invalid_indices_mask, 0.0) # shape (batch_size, context_len, context_len)

    # 两种查找最大值的方法：先按行查找，再按列查找，或者相反，都能得到最大值
    max_each_row = np.max(prob_dist, axis=-1) # shape (batch_size, context_len)
    max_each_col = np.max(prob_dist, axis=-2) # shape (batch_size, context_len)
    # 最大值所在的行为start_pos
    start_pos = np.argmax(max_each_row,axis=-1)
    # 最大值所在的列为end_pos
    end_pos = np.argmax(max_each_col, axis=-1)

    return start_pos, end_pos


def get_start_end_pos_with_start_not_greater_end_constraint(start_dist, end_dist):
    """start的位置不能超过end位置,

    TODO: 寻找可能更好的实现，目前loop的代码比较丑陋，
    在典型的context_len=400时，由于线性算法复杂度，虽然有很多loop，但是已经超过上一个矩阵化的算法速度数倍.
    在只需要start <= end的时候可以选择这个方法。

    Args:
        start_dist: start 位置的概率分布 np.ndarray shape (batch_size, context_len)
        end_dist: 位置的概率分布 np.ndarray shape (batch_size, context_len)

    Returns:
        start_pos: np.ndarray shape (batch_size,)
        end_pos: np.ndarray shape (batch_size,)
    """
    n, l = start_dist.shape
    assert np.array_equal(start_dist.shape, end_dist.shape)
    left_argmax = np.zeros(shape=start_dist.shape, dtype=np.int16)
    right_argmax = np.full(shape=end_dist.shape, fill_value=l-1, dtype=np.int16)

    for i in range(n):
        for j in range(l-1):
            left_max_idx = left_argmax[i, j]
            if start_dist[i, j+1] > start_dist[i, left_max_idx]:
                left_argmax[i, j+1] = j+1
            else:
                left_argmax[i, j+1] = left_max_idx

            right_max_idx = right_argmax[i, l-j-1]
            if end_dist[i, l-j-2] > end_dist[i, right_max_idx]:
                right_argmax[i, l-j-2] = l-j-2
            else:
                right_argmax[i, l-j-2] = right_max_idx

    start_pos_list = []
    end_pos_list = []
    for i in range(n):
        max_prob = -1.0
        max_start_idx = 0
        max_end_idx = 0
        for j in range(l-1):
            left_max_idx = left_argmax[i, j]
            right_max_idx = right_argmax[i, j]
            max_start = start_dist[i, left_max_idx]
            max_end = end_dist[i, right_max_idx]
            prob = max_start * max_end
            if max_prob < prob:
                max_prob = prob
                max_start_idx = left_max_idx
                max_end_idx = right_max_idx
        start_pos_list.append(max_start_idx)
        end_pos_list.append(max_end_idx)
    return np.array(start_pos_list), np.array(end_pos_list)


def linear_decay_learning_rate(steps, warmup_steps, factor):
    """来自 Attention is All You Need"""
    steps_float = tf.to_float(steps)
    warmup_learning_rate = steps_float * tf.rsqrt(tf.to_float(tf.pow(warmup_steps, 3)))
    learning_rate = factor * tf.minimum(tf.rsqrt(steps_float), warmup_learning_rate)
    return learning_rate


def log_growth_learning_rate(steps, warmup_steps, learning_rate):
    """从0开始，增加到learning rate. inverse exponential increase"""
    steps_float = tf.to_float(steps)
    warmup_steps_float = tf.to_float(warmup_steps)
    return tf.where(steps_float < warmup_steps,
                    learning_rate * tf.log(steps_float + 1.0) / tf.log(warmup_steps_float),
                    learning_rate)


def word_char_level_embedding(inputs,
                              word_embedding_size,
                              l2_lambda=3e-7,
                              kernel_size=5,
                              activation=tf.tanh,
                              use_bias=True,
                              name="WordCharLevelEmbedding"):
    """通过卷积，非线性，和最大池化，得到每个word token在character level上的embedding

    Args:
        inputs: Character level representation. Tensor shape(batch_size, seq_len, character_len, char_embedding_size)
        word_embedding_size: Int. The embedding vector size for each word
        l2_lambda: Float. lambda of l2 regularizer
        kernel_size: An integer or tuple/list of a single integer, specifying the
          length of the 1D convolution window.
        activation: Activation function. Set it to None to maintain a
          linear activation.
        use_bias: Boolean, whether the layer uses a bias.
        name: name of the scope

    Returns:
        outputs: Word Character level embedding. Tensor shape(batch_size, seq_len, word_embedding_size)

    """
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        _, seq_len, char_len, char_embedding_size = inputs.shape.as_list()
        inputs = tf.reshape(inputs, shape=(-1, char_len, char_embedding_size))
        outputs = tf.layers.conv1d(inputs,
                                   filters=word_embedding_size,
                                   kernel_size=kernel_size,
                                   kernel_initializer=initializer(),
                                   kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_lambda),
                                   activation=activation,
                                   use_bias=use_bias)
        word_embedding = tf.reduce_max(outputs, axis=-2)
        return tf.reshape(word_embedding, shape=(-1, seq_len, word_embedding_size))


def gelu(x):
    """Gaussian Error Linear Unit

    https://arxiv.org/abs/1606.08415

    Args:
        x: Tensor
    Returns:
        GELU(x).
    """
    return 0.5 * x * (1 + tf.tanh(math.sqrt(2/math.pi) * (x + 0.044715 * tf.pow(x, 3))))


def highway(inputs, num_layers, keep_prob, l2_lambda, activation=tf.nn.relu, name="highway"):
    """https://arxiv.org/abs/1505.00387

    Args:
        inputs: Tensor shape (batch_size, seq_len, vec_size)
        num_layers: the number of layers of highway network
        keep_prob: Tensor containing a single scalar that is the keep probability (for dropout)
        l2_lambda: Float. lambda of l2 regularizer
        activation: activation function for the hiddens
        name: scope name
    Returns:
        outputs: Tensor shape (batch_size, seq_len, vec_size)
    """
    vec_size = inputs.shape.as_list()[-1]
    outputs = inputs
    with vs.variable_scope(name, reuse=tf.AUTO_REUSE):
        for i in range(num_layers):
            with vs.variable_scope("layer_{0}".format(i+1)):
                transform_gate = tf.layers.dense(outputs, vec_size, activation=tf.sigmoid,
                                                 kernel_initializer=initializer_relu(),
                                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
                carry_gate = 1.0 - transform_gate

                h = tf.layers.dense(outputs, vec_size, activation,
                                          kernel_initializer=initializer_relu(),
                                          kernel_regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
                h = tf.nn.dropout(h, keep_prob)
                outputs = transform_gate * h + carry_gate * outputs

    return outputs


def conv_highway(inputs, output_size, num_layers, keep_prob, l2_lambda, activation=tf.nn.relu, name="conv_highway"):
    """Convolutional highway

    https://arxiv.org/abs/1505.00387

    Args:
        inputs: Tensor shape (batch_size, seq_len, vec_size)
        num_layers: the number of layers of highway network
        keep_prob: Tensor containing a single scalar that is the keep probability (for dropout)
        l2_lambda: Float. lambda of l2 regularizer
        activation: activation function for the h
        name: scope name
    Returns:
        outputs: Tensor shape (batch_size, seq_len, vec_size)
    """
    with vs.variable_scope(name, reuse=tf.AUTO_REUSE):
        outputs = conv1d(inputs,
                         output_size=output_size,
                         name="linear",
                         use_bias=False)
        for i in range(num_layers):
            with vs.variable_scope("layer_{0}".format(i+1)):
                transform_gate = conv1d(outputs,
                                        output_size=output_size,
                                        activation=tf.sigmoid,
                                        name="transform_gate",
                                        l2_lambda=l2_lambda)
                carry_gate = 1.0 - transform_gate

                h = conv1d(outputs,
                           output_size=output_size,
                           activation=activation,
                           name="hidden",
                           l2_lambda=l2_lambda)
                h = tf.nn.dropout(h, keep_prob)
                outputs = transform_gate * h + carry_gate * outputs

    return outputs

