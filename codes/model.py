# -*- coding: utf-8 -*-
import tensorflow as tf

class model(object):
    """base class of different models

    为不同的模型约定一个基本的框架:
    - 构建模型的图： build_graph
    - 输入数据拟合： fit
    - 模型的存储:
    - tensorboard: summary
    - 由输入预测输出: predict
    - 评估模型的性能，在这个任务中用EM和F1作为指标： evaluate
    """
    def build_graph(self):
        """创建模型对应的graph, init 时调用"""
        self._add_placeholder()
        self._add_embedding_layer()
        pred_start, pred_end = self._add_prediction_op()
        self.loss = self._add_loss_op(pred_start, pred_end)
        self.train_op = self._add_training_op(self.loss)

    # Build Graph
    def _add_placeholder(self):
        pass

    def _add_embedding_layer(self):
        pass

    def _create_feed_dict(self, inputs_batch, labels_batch=None, dropout=0):
        pass

    def _add_prediction_op(self):
        pass

    def _add_loss_op(self, pred_start, pred_end):
        raise NotImplementedError("模型需要实现loss")

    def _add_training_op(self, loss):
        raise NotImplementedError("模型需要实现training_op")

    def _add_saver(self):
        pass

    def _add_summary(self):
        pass


    ## 提供的功能
    def fit(self, session, train_context_path, train_qn_path, train_ans_path, dev_qn_path, dev_context_path, dev_ans_path):
        """fit data"""

    def _train_on_batch(self):
        """输入batch进行训练

        Returns:
            loss: The loss (averaged across the batch) for this batch.
            global_step: The current number of training iterations we've done
            param_norm: Global norm of the parameters
            gradient_norm: Global norm of the gradients
        """
        pass

    def _run_on_batch(self, session, batch, summary_writer):
        """执行每个batch需要的工作

        工作内容为：
        - 记录一次batch训练的时间
        - 对计算结果进行处理（主要是loss的指数平滑）
        - 在特定次数时保存模型到文件中
        - 在指定次数时，在dev上进行评估，并保存summary

        Args:
        """

    def evaluate(self):
        pass

    def predict(self, x, batch_size=None, verbose=0, steps=None):
        pass
