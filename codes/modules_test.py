# *_* encoding:utf-8 *_*
from modules import *
from tensorflow import test
import numpy as np

class SimilarityMatrixTest(tf.test.TestCase):
    def test_get_similarity_matrix(self):
        c = tf.constant(np.random.randn(20,400,80), dtype=np.float32)
        q = tf.constant(np.random.randn(20,35,80), dtype=np.float32)
        initializer = tf.ones_initializer
        l2_lambda =3e-7
        s1 = get_similarity_matrix(context=c, query=q, l2_lambda=l2_lambda,
                                   scope="s1",
                                   weight_initializer=initializer,
                                   use_bias=False)
        s2 = get_similarity_matrix(context=c,
                                   query=q,
                                   scope="s2",
                                   l2_lambda=l2_lambda,
                                   weight_initializer=initializer,
                                   use_bias=True,
                                   bias_initializer=tf.zeros_initializer)

        def naive_similarity_matrix(c, q, l2_lambda, scope, initializer, use_bias, bias_initializer=None):
            _, c_len, vec_size = c.shape.as_list()
            _, q_len, q_vec_size = q.shape.as_list()
            assert vec_size == q_vec_size

            with tf.variable_scope(scope):
                # 计算 Similarity Matrix
                c_aug = tf.tile(tf.expand_dims(c, axis=2), [1, 1, q_len, 1])  # shape (batch_size, c_len, q_len, vec_size)
                q_aug = tf.tile(tf.expand_dims(q, axis=1), [1, c_len, 1, 1])  # shape (batch_size, c_len, q_len, vec_size)
                # 计算score
                c = tf.concat([c_aug, q_aug, c_aug * q_aug], axis=-1)
                Ws = tf.get_variable("Ws", shape=(3 * vec_size,), initializer=initializer,
                                     regularizer=tf.contrib.layers.l2_regularizer(l2_lambda))
                similarity_score = tf.tensordot(c, Ws, axes=[[-1], [0]])  # shape (batch_size, num_keys, num_values)

                if use_bias:
                    bias = tf.get_variable("bias", shape=(q_len,), initializer=bias_initializer, dtype=c.dtype)
                    return tf.nn.bias_add(similarity_score , bias)
                else:
                    return similarity_score

        naive_s1 = naive_similarity_matrix(c, q, l2_lambda, "ns1", initializer, False)
        naive_s2 = naive_similarity_matrix(c, q, l2_lambda, "ns2", initializer, True, tf.zeros_initializer)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            _s1, _s2, _naive_s1, _naive_s2 = sess.run([s1, s2, naive_s1, naive_s2])
            self.assertEqual(_s1.shape, (20, 400, 35))
            self.assertEqual(_s2.shape, (20, 400, 35))

            # tolerance 是不是有些偏大?
            self.assertAllClose(_s1, _naive_s1, rtol=1e-4, atol=1e-4)
            self.assertAllClose(_s2, _naive_s2, rtol=1e-4, atol=1e-4)


class BiDafAttentionTest(tf.test.TestCase):
    def test_bidaf_attn(self):
        v = tf.constant(np.random.randn(10,30,80), dtype=np.float32)
        k = tf.constant(np.random.randn(10,50,80), dtype=np.float32)
        v_mask = tf.constant(np.random.rand(10,30) > 0.5)
        k_mask = tf.constant(np.random.rand(10,50) > 0.5)
        attn = BiDirectionalAttn(0.9, 3e-8)
        output  = attn.build_graph(k, v, k_mask, v_mask)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            k2v_dist, v2k_dist, k2v_attn, v2k_attn = sess.run(output)
            self.assertEqual(k2v_dist.shape, (10, 50, 30))
            self.assertEqual(v2k_dist.shape, (10, 50))
            self.assertEqual(k2v_attn.shape, (10, 50, 80))
            self.assertEqual(v2k_attn.shape, (10, 1, 80))


class DynamicCoAttentionTest(tf.test.TestCase):
    def test_dynamic_coattention(self):
        v = tf.constant(np.random.randn(10,30,80), dtype=np.float32)
        k = tf.constant(np.random.randn(10,50,80), dtype=np.float32)
        v_mask = tf.constant(np.random.rand(10,30) > 0.5)
        k_mask = tf.constant(np.random.rand(10,50) > 0.5)
        attn = DynamicCoAttention(0.9, 3e-8)
        output  = attn.build_graph(k, v, k_mask, v_mask)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            k2v_dist, v2k_dist, k2v_attn, v2k_attn = sess.run(output)
            self.assertEqual(k2v_dist.shape, (10, 50, 30))
            self.assertEqual(v2k_dist.shape, (10, 50, 50))
            self.assertEqual(k2v_attn.shape, (10, 50, 80))
            self.assertEqual(v2k_attn.shape, (10, 50, 80))

class MultiHeadAttnTest(tf.test.TestCase):
    """测试MultiHeadAttention是否正确实现了"""
    def test_multi_head_attn(self):
        q = tf.constant(np.random.randn(10,20,70), dtype=np.float32)
        k = tf.constant(np.random.randn(10,30,70), dtype=np.float32)
        v = tf.constant(np.random.randn(10,30,80), dtype=np.float32)
        v_mask = tf.constant(np.random.rand(10,30) > 0.5)
        attn = MultiHeadAttn(head_vec_size=30, num_heads=10, keep_prob_attn=0.9,l2_lambda=0.0009)
        _, attn_output = attn.build_graph(q, k, v, v_mask)
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            self.assertEqual(attn_output.shape, (10,20,300))

    def test_split_head(self):
        """参考cs224n lecture12 和 Attention is All You Need,
         这里有一点不同，对应的不是head_i, 而是head_i_v未经过Attention Layer的inputs"""
        q = tf.constant(np.random.randn(12,8,150), dtype=np.float32)
        q_heads = split_head(q, num_heads=10)

        k = tf.constant(np.arange(16).reshape(2,2,4), dtype=np.float32)
        k_heads = split_head(k, 4)
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            self.assertEqual(q_heads.shape, (12,10,8,15))
            self.assertEqual(k_heads.shape, (2,4,2,1))

            # expected_k_heads = np.arange(16).reshape(2,4,2,1)
            # self.assertAllEqual(sess.run(k_heads), expected_k_heads)

    def test_scale_dot_product_attn(self):
        q = tf.constant(np.random.randn(10,4,7,13), dtype=np.float32)
        k = tf.constant(np.random.randn(10,4,30,13), dtype=np.float32)
        v = tf.constant(np.random.randn(10,4,30,18), dtype=np.float32)
        v_mask = tf.constant(np.random.rand(10,4,30) > 0.5)
        attn = ScaledDotProductAttention(0.9)
        dist, output = attn.build_graph(q, k, v, v_mask)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            o = sess.run(output)
            self.assertEqual(o.shape, (10,4,7,18))

    # def test_scale_dot_prodcut_attn_output(self):
    #     q = tf.constant(np.ones(3,4,3,2), dtype=tf.float32)
    #     k = tf.constant()


class GetStartEndPosTest(tf.test.TestCase):
    def test_get_start_end_pos_with_max_length_constraint(self):
        max_length = 1
        start_dist = np.array([[0.3, 0.4, 0.2, 0.1], [0.1, 0.3, 0.0, 0.6]])
        end_dist = np.array([[0.1, 0.5, 0.2, 0.2], [0.2, 0.2, 0.4, 0.2]])
        start_pos, end_pos = get_start_end_pos_with_max_length_constraint(start_dist, end_dist, max_length)

        self.assertAllEqual(start_pos, (1,1))
        self.assertAllEqual(end_pos, (1,2))

    def test_get_start_end_pos_with_start_not_greater_end_constraint(self):
        start_dist = np.array([[0.3, 0.4, 0.2, 0.1, 0.3, 0.2, 0.15], [0.1, 0.3, 0.0, 0.6, 0.2, 0.2, 0.3]])
        end_dist = np.array([[0.1, 0.5, 0.2, 0.2, 0.2, 0.1, 0.1], [0.2, 0.2, 0.4, 0.2, 0.3, 0.3, 0.3]])
        start_pos, end_pos = get_start_end_pos_with_start_not_greater_end_constraint(start_dist, end_dist)

        self.assertAllEqual(start_pos, (1,3))
        self.assertAllEqual(end_pos, (1,6))

    # def test_time(self):
    #     start_dist = np.random.randn(100, 400)
    #     end_dist = np.random.randn(100, 400)
    #
    #     import time
    #     tic = time.time()
    #     start_pos , end_pos = get_start_end_pos_with_max_length_constraint(start_dist, end_dist, 0)
    #     print ("v1 elapse : {0}".format(time.time() - tic))
    #
    #     tic = time.time()
    #     start_pos , end_pos = get_start_end_pos_with_start_not_greater_end_constraint(start_dist, end_dist)
    #     print ("v2 elapse : {0}".format(time.time() - tic))


class LayerNormTest(tf.test.TestCase):
    def test_layer_norm(self):
        x = tf.constant([[1.0, 2.0, 3.0], [-2.0, 0.0, 2.0]])
        norm_x = layer_normalization(x)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            nx = sess.run(norm_x)
            self.assertShapeEqual(nx, x)
            self.assertAllClose(nx, [[-1.224744, 0.0, 1.224744], [-1.224744, 0.0, 1.224744]])


class TransformerEncoderBlockTest(tf.test.TestCase):
    def test_encoder(self):
        inputs = tf.constant(np.random.randn(37, 40, 60), dtype=tf.float32)
        inputs_mask = tf.constant(np.random.rand(37, 40) > 0.5, dtype=tf.bool)
        encoder = TransformerEncoderBlock(model_size=60,
                                          ffn_size=150,
                                          num_conv_layer=2,
                                          num_heads=5,
                                          kernel_size=7,
                                          l2_lambda=5e-3,
                                          keep_prob=0.9,
                                          last_layer_survival_prob=0.9,
                                          total_layer=2+2)
        output = encoder.build_graph(inputs, inputs_mask, start_layer=1)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            o = sess.run(output)
            self.assertEqual(o.shape, (37, 40, 60))


class StackedTransformerEncoderBlockTest(tf.test.TestCase):
    def test_stacked_transformer_block_encoder(self):
        inputs = tf.constant(np.random.randn(37, 40, 60), dtype=tf.float32)
        inputs_mask = tf.constant(np.random.rand(37, 40) > 0.5, dtype=tf.bool)
        encoder = StackedTransformerEncoderBlock(num_stacked=3,
                                                 model_size=60,
                                                 ffn_size=150,
                                                 num_conv_layer=2,
                                                 num_heads=5,
                                                 kernel_size=7,
                                                 l2_lambda=5e-3,
                                                 keep_prob=0.9,
                                                 name="test_stacked_encoder",
                                                 last_layer_survival_prob=0.9)
        output = encoder.build_graph(inputs, inputs_mask)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            o = sess.run(output)
            self.assertEqual(o.shape, (37, 40, 60))



class PositionalEncodingTest(tf.test.TestCase):
    def test_positional_encoding(self):
        inputs = tf.constant(np.random.randn(30, 20, 50), dtype=tf.float32)
        encoding = positional_encoding(inputs)

        with self.test_session() as sess:
            o = sess.run(encoding)
            self.assertEqual(o.shape, (30,20,50))


class WordCharacterLevelEmbeddingTest(tf.test.TestCase):
    def test_word_character_level_embedding(self):
        inputs = tf.constant(np.random.randn(20,200,35,25), dtype=tf.float32)
        outputs = word_char_level_embedding(inputs, word_embedding_size=100)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            o = sess.run(outputs)
            self.assertEqual(o.shape, (20,200,100))


class GELUTest(tf.test.TestCase):
    def test_gelu(self):
        inputs = tf.constant(np.random.randn(2,2,10,4),dtype=tf.float32)
        outputs = gelu(inputs)

        with self.test_session() as sess:
            o = sess.run(outputs)
            self.assertEqual(o.shape, (2,2,10,4))


class HighwayTest(tf.test.TestCase):
    def test_higyway_layer(self):
        inputs = tf.constant(np.random.randn(30, 20, 50), dtype=tf.float32)
        outputs = highway(inputs, 1, 0.9, 3e-7)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            o = sess.run(outputs)
            self.assertEqual(o.shape, (30,20,50))


class SeparableConv1dTest(tf.test.TestCase):
    def test_separable_conv1d(self):
        inputs = tf.constant(np.random.randn(30, 20, 50), dtype=tf.float32)
        outputs = separable_conv1d(inputs, filters=128, kernel_size=7, l2_lambda=3e-7)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            o = sess.run(outputs)
            self.assertEqual(o.shape, (30,20,128))


if __name__ == "__main__":
    tf.test.main()

