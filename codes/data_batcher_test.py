# coding=utf8

import unittest
from tensorflow import test
import numpy as np

from data_batcher import *

class PaddedCharactersTest(unittest.TestCase):
    def setUp(self):
        self.char_ids_1 = [[0,3,2], [0,1,2,4,5]]
        self.char_ids_2 = [[1,2,4,10], [56,4,2,5], [5,2,3]]
        self.char_ids_3 = [[2,4,6,2,15,4,5,4],[39,3,3,60,10],[12,3,2,5,7,76], [3,1,4,65,7]]

        self.char_ids_batch = [self.char_ids_1, self.char_ids_2, self.char_ids_3]

    def test_padded_characters_with_using_start_end_id_True(self):
        result = padded_characters(self.char_ids_batch, batch_pad=4, max_word_length=4, using_start_end_id=True)

        p = CHAR_PAD_ID
        self.assertEqual(result[0], [[0,3,2,p,p,p], [0,1,2,4,5,p], [p]*6, [p] * 6])
        self.assertEqual(result[1], [[1,2,4,10,p,p], [56,4,2,5,p,p], [5,2,3,p,p,p], [p] * 6])
        self.assertEqual(result[2], [[2,4,6,2,15,4],[39,3,3,60,10,p],[12,3,2,5,7,76], [3,1,4,65,7,p]])
        self.assertEquals(len(result), len(self.char_ids_batch))

    def test_padded_characters_with_using_start_end_id_False(self):
        result = padded_characters(self.char_ids_batch, batch_pad=5, max_word_length=4, using_start_end_id=False)

        p = CHAR_PAD_ID
        self.assertEqual(result[0], [[0,3,2,p], [0,1,2,4], [p]*4, [p]*4, [p]*4])
        self.assertEqual(result[1], [[1,2,4,10], [56,4,2,5], [5,2,3,p], [p]*4, [p]*4])
        self.assertEqual(result[2], [[2,4,6,2],[39,3,3,60],[12,3,2,5], [3,1,4,65], [p]*4])
        self.assertEquals(len(result), len(self.char_ids_batch))

if __name__ == '__main__':
    unittest.main()
