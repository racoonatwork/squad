# -*- coding:utf-8 -*-
import tensorflow as tf

from qa_model import QAModel
from modules import SimpleSoftmaxLayer, DynamicCoAttention, TransformerEncoderBlock, separable_conv1d, \
    StackedTransformerEncoderBlock
from modules import get_start_end_pos_with_max_length_constraint, get_start_end_pos_with_start_not_greater_end_constraint


class TransformerModel(QAModel):
    """一个添加了separable conv 的 transform encoder，并加

    结构参见 QANet https://arxiv.org/pdf/1804.09541.pdf
    """

    def build_graph(self):
        """构建model的graph，模型和QANet中的一样"""

        with tf.variable_scope("embedding_encoder"):
            # Map the embeddings vector to model size by conv layer.
            context_inputs = separable_conv1d(inputs=self.context_embs,
                                              filters=self.FLAGS.encoder_model_size,
                                              l2_lambda=self.FLAGS.l2_lambda,
                                              kernel_size=self.FLAGS.embedding_kernel_size,
                                              name="context_inputs_conv1d")

            question_inputs = separable_conv1d(inputs=self.qn_embs,
                                               filters=self.FLAGS.encoder_model_size,
                                               l2_lambda=self.FLAGS.l2_lambda,
                                               kernel_size=self.FLAGS.embedding_kernel_size,
                                               name="question_inputs_conv1d")

            stacked_embedding_encoder = self._get_stacked_encoder(
                num_stacked=self.FLAGS.num_stacked_embedding_encoder_block,
                num_conv_layer=self.FLAGS.num_conv_layer_embedding,
                kernel_size=self.FLAGS.embedding_kernel_size,
                name="stacked_embedding_encoder")
            context_encoding = stacked_embedding_encoder.build_graph(inputs=context_inputs,
                                                                     masks=self.context_mask,
                                                                     use_attn_bias=False)
            question_encoding = stacked_embedding_encoder.build_graph(inputs=question_inputs,
                                                                      masks=self.qn_mask,
                                                                      use_attn_bias=False)

        with tf.variable_scope("context-query_attention"):
            # context and question bi-directional attention
            attn = DynamicCoAttention(self.keep_prob, self.FLAGS.l2_lambda)
            _, _, c2q_attn_output, q2c_attn_output = attn.build_graph(keys=context_encoding,
                                                                      values=question_encoding,
                                                                      keys_mask=self.context_mask,
                                                                      values_mask=self.qn_mask)
            # Concat attn_output to context_encoding to get blended_reps
            blended_reps = tf.concat([context_encoding,
                                      c2q_attn_output,
                                      context_encoding * c2q_attn_output,
                                      context_encoding * q2c_attn_output],
                                     axis=-1)  # (batch_size, context_len, encoder_size*8)

        with tf.variable_scope("model_encoder"):
            # Map the attention output vector to model size by conv layer.
            model_inputs = separable_conv1d(inputs=blended_reps,
                                            filters=self.FLAGS.encoder_model_size,
                                            l2_lambda=self.FLAGS.l2_lambda,
                                            kernel_size=self.FLAGS.model_kernel_size,
                                            name="model_inputs_conv1d")

            stacked_model_encoder = self._get_stacked_encoder(
                num_stacked=self.FLAGS.num_stacked_model_encoder_block,
                num_conv_layer=self.FLAGS.num_conv_layer_model,
                kernel_size=self.FLAGS.model_kernel_size,
                name="stacked_model_encoder")
            # hard coding (total_stacked_model_encoder)
            total_stacked_model_encoder = 3
            model_outputs_list = []
            for i in range(total_stacked_model_encoder):
                model_outputs = stacked_model_encoder.build_graph(inputs=model_inputs,
                                                                  masks=self.context_mask)
                model_outputs_list.append(model_outputs)
                model_inputs = model_outputs
            assert len(model_outputs_list) == 3, "Hard Coding! model_outpust_list's length:{0} must be equal 3!".format(
                len(model_outputs_list))
            model_output1, model_output2, model_output3 = model_outputs_list

        # Use softmax layer to compute probability distribution for start location
        # Note this produces self.logits_start and self.probdist_start, both of which have shape (batch_size, context_len)
        with tf.variable_scope("StartDist"):
            start_inputs = tf.concat([model_output1, model_output2], axis=-1)
            softmax_layer_start = SimpleSoftmaxLayer(self.FLAGS.l2_lambda)
            self.logits_start, self.probdist_start = softmax_layer_start.build_graph(start_inputs, self.context_mask)
            # self.logits_start = tf.Print(self.logits_start, [tf.argmax(self.logits_start, 1)], "argmax(logits_start) =", summarize=20, first_n=10)

        # Use softmax layer to compute probability distribution for end location
        # Note this produces self.logits_end and self.probdist_end, both of which have shape (batch_size, context_len)
        with tf.variable_scope("EndDist"):
            end_inputs = tf.concat([model_output1, model_output3], axis=-1)
            softmax_layer_end = SimpleSoftmaxLayer(self.FLAGS.l2_lambda)
            self.logits_end, self.probdist_end = softmax_layer_end.build_graph(end_inputs, self.context_mask)
            # self.logits_end = tf.Print(self.logits_start, [tf.argmax(self.logits_end, 1)], "argmax(logits_end) =", summarize=20, first_n=10)

    def add_loss(self):
        """
        Add loss computation to the graph.

        Uses:
          self.logits_start: shape (batch_size, context_len)
            IMPORTANT: Assumes that self.logits_start is masked (i.e. has -large in masked locations).
            That's because the tf.nn.sparse_softmax_cross_entropy_with_logits
            function applies softmax and then computes cross-entropy loss.
            So you need to apply masking to the logits (by subtracting large
            number in the padding location) BEFORE you pass to the
            sparse_softmax_cross_entropy_with_logits function.

          self.ans_span: shape (batch_size, 2)
            Contains the gold start and end locations

        Defines:
          self.loss_start, self.loss_end, self.loss: all scalar tensors
        """
        with tf.variable_scope("loss"):
            # Calculate loss for prediction of start position
            loss_start = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits_start,
                                                                        labels=self.ans_span[:,
                                                                               0])  # loss_start has shape (batch_size)
            self.loss_start = tf.reduce_mean(loss_start)  # scalar. avg across batch
            tf.summary.scalar('loss_start', self.loss_start)  # log to tensorboard

            # Calculate loss for prediction of end position
            loss_end = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits_end,
                                                                      labels=self.ans_span[:, 1])
            self.loss_end = tf.reduce_mean(loss_end)
            tf.summary.scalar('loss_end', self.loss_end)

            # l2_loss
            # variables = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
            # regularizer = tf.contrib.layers.l2_regularizer(scale=self.FLAGS.l2_lambda)
            # self.l2_loss = tf.contrib.layers.apply_regularization(regularizer, variables)
            self.l2_loss = tf.losses.get_regularization_loss()
            tf.summary.scalar('l2_loss', self.l2_loss)

            # Add the two losses and l2_loss
            self.loss = self.loss_start + self.loss_end + self.l2_loss
            tf.summary.scalar('loss', self.loss)

    def _get_stacked_encoder(self, num_stacked, num_conv_layer, kernel_size, name):
        """返回堆叠encoder, 所有堆叠的encoder都reuse变量,

         helper函数，避免太多的重复参数输入

        Args:
            num_stacked: int. 堆叠的数量
            num_conv_layer: int. number of the conv layers in the encoder.
            kernel_size: int.
            name: the name of scope

        Returns:
            outputs: Tensor shape (batch_size, seq_len, input_size)
        """
        return StackedTransformerEncoderBlock(num_stacked=num_stacked,
                                              model_size=self.FLAGS.encoder_model_size,
                                              ffn_size=self.FLAGS.encoder_fnn_size,
                                              num_conv_layer=num_conv_layer,
                                              num_heads=self.FLAGS.num_heads,
                                              keep_prob=self.keep_prob,
                                              l2_lambda=self.FLAGS.l2_lambda,
                                              kernel_size=kernel_size,
                                              last_layer_survival_prob=self.last_layer_survival_prob,
                                              name=name)

    def get_start_end_pos(self, session, batch):
        """得到满足约束的start和end位置"""
        start_dist, end_dist = self.get_prob_dists(session, batch)
        if self.FLAGS.max_answer_len <= 0:
            return get_start_end_pos_with_start_not_greater_end_constraint(start_dist, end_dist)
        else:
            return get_start_end_pos_with_max_length_constraint(start_dist, end_dist, self.FLAGS.max_answer_len)
