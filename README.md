# [Squad](https://rajpurkar.github.io/SQuAD-explorer/)

这是一个自学项目，基于Squad v1.1数据集进行各种尝试。目前主要复现了2个论文中model:[Bidirectional Attention Flow for Machine Comprehension
](https://arxiv.org/abs/1611.01603)(简称**bidaf**) 和 [QANet](https://arxiv.org/abs/1804.09541). 持续开发中。。。

## 目录结构

- codes. 代码目录
- experiments. 保存checkpoints的目录.
- note. 读论文时的笔记总结，遇到的问题，以及部分实验记录和当时的想法。
- data. 下载和预处理的数据
- tool. 一些脚本，方便在aliyun上运行实验和传输数据
- docker. 目前没有用过，未来考虑使用docker。

## 环境需求

来自requirements文件

- colorama==0.3.9
- nltk==3.2.5
- numpy==1.14.0
- six==1.11.0
- tensorflow==1.4.1  # Change to tensorflow==1.4.1 if you need to run CPU-only tensorflow (e.g. on your laptop)
- tensorflow-tensorboard==0.4.0
- tqdm==4.19.5
- pandas=0.23.1
- matplotlib

## 使用

使用conda创建环境`squad`，并且下载数据集，并进行预处理。

```shell
./get_start.sh
```

训练和查看结果

```shell
# train
python main.py --experiment_name="YOUR EXPERIMENT NAME"

# tensorboard
tensorboard --logdir=experiments/.
```

## TODO

- [ ] 不再使用原有的预处理代码，改用tensorflow处理后的数据集，简化代码和流程。
- [ ] 接近QANet中的dev EM(73.6)。 目前~~55~~ -> 62.87 (F1 73.17)
- [ ] 使用Squad 2.0

## cs224n-win18-squad

该代码基于 [cs224n-win18-squad](https://github.com/abisee/cs224n-win18-squad.git)，其包含了对squad数据的预处理和一个baseline.以下为原来的README

Code for the Default Final Project (SQuAD) for [CS224n](http://web.stanford.edu/class/cs224n/), Winter 2018

Note: this code is adapted in part from the [Neural Language Correction](https://github.com/stanfordmlgroup/nlc/) code by the Stanford Machine Learning Group.

## Reference

- [QANet: Combining Local Convolution with Global Self-Attention for Reading Comprehension
](https://arxiv.org/abs/1804.09541)
- [Attention Is All You Need
](https://arxiv.org/abs/1706.03762)
- [Bidirectional Attention Flow for Machine Comprehension
](https://arxiv.org/abs/1611.01603)
- [Character-Aware Neural Language Models](https://arxiv.org/abs/1508.06615)
- [Xception: Deep Learning with Depthwise Separable Convolutions](https://arxiv.org/abs/1610.02357)
- [Dynamic Coattention Networks For Question Answering
](https://arxiv.org/abs/1611.01604)
- [Deep Networks with Stochastic Depth
](https://arxiv.org/abs/1603.09382)
- [Github QANet code](https://github.com/NLPLearn/QANet)