# coding=utf-8
"""
需要：ssh连接,依赖 scp, rsync
备份一些实验的log.txt和flags.json，避免远端空间不足被删除了，(使用scp)
另外在指定的情况下，会同步整个指定文件夹（使用rsync)
"""

import argparse
import os
from subprocess import call, check_call, check_output, CalledProcessError

from aliyun import IP, USER, REMOTE_EXPERIMENTS_DIR

SIMPLE_BACKUP_DIR = "../backup"
LOCAL_EXPERIMENTS_DIR = "../experiments"

EXPERIMENT_NAME = "" # 留空选择最新的tag名字


def make_sure_directory_exist(dir):
    """保证目录的存在，不存在就创建一个

    Args:
        dir: name of the directory

    """
    if not os.path.exists(dir):
        os.mkdir(dir)


def backup_simple(experiment, ip, user=USER):
    """简单备份， 只备份log.txt, flags.json

    Args:
        experiment: name of the experiment
        ip: ip host
        user: username of the remote host

    Returns:
        True: Done! False: Failed

    """
    target_dir = os.path.join(SIMPLE_BACKUP_DIR, experiment)
    make_sure_directory_exist(target_dir)

    remote_log = os.path.join(REMOTE_EXPERIMENTS_DIR, experiment, "log.txt")
    remote_flags = os.path.join(REMOTE_EXPERIMENTS_DIR, experiment, "flags.json")

    returncode = scp(remote_log, target_dir, ip, user)
    if returncode == 0:
        returncode = scp(remote_flags, target_dir, ip, user)
    return returncode == 0


def sync_experiment(experiment, ip, user=USER, copy_best_checpoint=False):
    """同步整个实验文件夹

    Args:
        experiment:

    Returns:

    """
    source_file = os.path.join(REMOTE_EXPERIMENTS_DIR, experiment)
    target_dir = os.path.join(LOCAL_EXPERIMENTS_DIR, experiment)
    return rsync(source_file, target_dir, ip, user) == 0


def rsync(source_dir, target_dir, ip, user="root"):
    try:
        args = ["rsync", "-av", "--progress"]
        cmd = user + "@" + ip + ":"
        cmd += source_dir
        args.append(cmd)
        args.append(target_dir)

        return check_call(args=args)
        print ("rsync from {0} to {1} done!".format(source_dir, target_dir))
    except CalledProcessError as e:
        print ("rsync error: return code: {0}, output:{1}".format(e.returncode, e.output))
        return e.returncode


def scp(source_file, target_dir, ip, user="root"):
    try:
        args = ["scp", "-r"]
        cmd = user + "@" + ip + ":"
        cmd += source_file
        args.append(cmd)
        args.append(target_dir)

        return check_call(args=args)
        print ("scp from {0} to {1} done!".format(source_file, target_dir))
    except CalledProcessError as e:
        print ("scp error: return code: {0}, output:{1}".format(e.returncode, e.output))
        return e.returncode


def find_latest_experiment_name_from_git():
    """未经充分测试，仅适用于我的环境

    实现来自讨论
    https://stackoverflow.com/questions/1404796/how-to-get-the-latest-tag-name-in-current-branch-in-git

    Returns: 最近的一个tag 名字，对分支和标注有什么要求，没有测试过

    """
    try:
        args = ["git", "describe", "--tags", "--abbrev=0"]

        output = check_output(args=args)
        # 多行只取第一行
        return output.split('\n')[0].strip()

    except CalledProcessError as e:
        print ("find lastest experiment name error! return code: {0}, output:{1}".format(e.returncode, e.output))
        return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", help="ip地址")
    parser.add_argument("--user", "-u")
    parser.add_argument("--cpall", "-a", action='store_true', default=False, help="备份整个文件夹，默认关闭")
    parser.add_argument("--name", "-n", help="实验名字")

    args = parser.parse_args()
    experiment_name = args.name if args.name else find_latest_experiment_name_from_git()
    experiment_name = experiment_name if experiment_name else EXPERIMENT_NAME
    if not experiment_name:
        print ("需要提供备份的实验名字")
        return None
    else:
        print ("experiment name: [" + experiment_name + "]")

    ip = args.ip if args.ip else IP
    if not ip:
        print ("需要ip地址")
        return None

    if args.cpall:
        OK = sync_experiment(experiment_name, ip=ip)
    else:
        OK = backup_simple(experiment_name, ip=ip)

    if OK:
        print ("Back up {0} Done!!!".format(experiment_name))
    else:
        print ("Bacu up {0} Failed!!!".format(experiment_name))



if __name__ == '__main__':
    main()
