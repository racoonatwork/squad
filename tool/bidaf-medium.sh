experiment_name=bidaf_medium_char_lstm_modeling_2highway
# if [[ "$#" -gt 0 && "$1"=="-tag" ]]
# then
    # git tag $experiment_name
# else
    # echo "only support -tag"
# fi

python ../codes/main.py --mode=train --experiment_name=$experiment_name --model_name="bidaf" --embedding_size=100 --batch_size=50 --hidden_size=100 --context_len=400 --num_modeling_layer=2 --num_highway_layer=2
