# coding=utf-8

"""
读取experiments/{experiment_name}/log.txt中的数据，用于统计batch-time等数据
"""
import os
import argparse

EXPERIMENTS_PATH = "../experiments"

def read_log_file(experiment_name=None):
    """ read log.txt file in the experiment with specific name.

    Args:
        experiment_name: name of the experiment. if None, read all experiments under the EXPERIMENTS_PATH.

    Returns:

    """
    if experiment_name:
        dirs = [experiment_name]
    else:
        dirs = [d for d in os.listdir(EXPERIMENTS_PATH) if os.path.isdir(os.path.join(EXPERIMENTS_PATH, d))]

    log_filename = os.path.join(EXPERIMENTS_PATH, experiment_name)
    with open(log_filename, 'r') as log:
        log.readline()


def read_log_line(line):
    pass


def process_epoch_line(line):
    pass


def process_evaluate_line(line):
    pass


def markdown_table_for_vscode_report(report):
    pass


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", help="output dir")
    pass